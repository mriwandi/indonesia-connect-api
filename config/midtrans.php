<?php

return [
  'is_production' => env('MIDTRANS_ENV') === 'PRODUCTION' ? true : false,
  'server_key' => env('MIDTRANS_ENV') === 'PRODUCTION' ? env('MIDTRANS_SERVER_KEY') : env('MIDTRANS_SB_SERVER_KEY'),
  'client_key' => env('MIDTRANS_ENV') === 'PRODUCTION' ? env('MIDTRANS_CLIENT_KEY') : env('MIDTRANS_SB_CLIENT_KEY'),
  'is_sanitized' => env('MIDTRANS_IS_SANITIZED', false),
  'is_3ds' => env('MIDTRANS_IS_3DS', false),
  'endpoint' => env('MIDTRANS_ENV') === 'PRODUCTION' ? 'https://app.midtrans.com/snap/v1/transactions' : 'https://app.sandbox.midtrans.com/snap/v1/transactions'
];