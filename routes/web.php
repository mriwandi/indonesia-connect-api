<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return response('Not Found',404);
});

//fix
$router->post('login', 'LoginController@index');
$router->post('register', 'RegisterController@index');
$router->get('/user/cek-block/{idUser}', 'LoginController@cekBlock');

$router->get('feeds/{idUser}/{offset}/{limit}', 'FeedController@index');

//belum fix
$router->get('follower/{id}', 'FollowerController@follower');
$router->get('following/{id}', 'FollowerController@following');
$router->post('follows', 'FollowerController@follows');
$router->post('unfollow', 'FollowerController@unfollow');

$router->get('list-message/{id}', 'MessagesController@getMessage');
//$router->get('message/{id}','MessagesController@get');
$router->get('/message/detail/{idRoom}', 'MessagesController@getDetailMessage');
$router->post('message/send/{idUser}', 'MessagesController@send');
$router->post('message/new/{idUser}', 'MessagesController@newMessage');

$router->get('profile/{idUser}', 'LoginController@getProfile');
$router->post('profile/update', 'LoginController@updateProfile');
$router->post('profile/search', 'FollowerController@search');
$router->get('my-activity/{idUser}/{offset}/{limit}', 'FeedController@myActivity');

$router->get('aksi/top/{idUser}', 'AksiController@topAksi');
$router->get('aksi/new/{idUser}', 'AksiController@newAksi');
$router->get('aksi/highlight/{idUser}', 'AksiController@highlightAksi');
$router->get('aksi/detail/{idAksi}', 'DetailAksiController@getData');
$router->post('aksi/donate', 'DetailAksiController@donate');
$router->post('aksi/volunteering', 'DetailAksiController@volunteering');
$router->get('aksi/report/{idAksi}', 'DetailAksiController@getReport');
$router->post('aksi/insert', 'DetailAksiController@insertAksi');
$router->get('aksi/cek-block/{idAksi}', 'DetailAksiController@cekBlock');
$router->post('cek-relasi', 'FollowerController@cekRelasi');
//$router->post('message/{id}', 'MessagesController@getPerUser');
$router->get('aksi/detail/{idAksi}/sub-aksi/{idSubAksi}/{idUser}', 'DetailAksiController@subAksiDetail');
$router->post('aksi/edit', 'DetailAksiController@ubahAksi');

$router->get('aksi/usul/{idAksi}/{idUser}', 'DetailAksiController@usul');
$router->post('aksi/usul/create', 'DetailAksiController@insertUsul');
$router->post('aksi/search/top', 'AksiController@searchTopAksi');
$router->post('aksi/search/newest', 'AksiController@searchNewAksi');
$router->post('aksi/search/highlight', 'AksiController@searchHighlightAksi');
$router->post('aksi/usul/upvote', 'DetailAksiController@upvote');
$router->post('aksi/usul/unvote', 'DetailAksiController@unvote');
$router->post('aksi/usul/reply', 'DetailAksiController@replyUsul');

$router->get('grup/{idUser}', 'GroupController@listGrup');
$router->get('grup/feeds/{idGrup}', 'GroupController@feeds');
$router->get('grup/feeds/member/{idGrup}', 'GroupController@memberFeeds');
$router->post('grup/insert', 'GroupController@create');
$router->get('grup/detail/{idGrup}/{idUser}', 'GroupController@detail');
$router->get('grup/list-member/{idGrup}', 'GroupController@listMember');
$router->post('grup/join', 'GroupController@join');
$router->post('grup/keluar', 'GroupController@keluar');
$router->post('grup/edit', 'GroupController@updateGrup');
$router->post('grup/search', 'GroupController@search');

$router->get('manage/list-aksi/{idUser}', 'ManageAksiController@listAksi');
$router->get('manage/list-subaksi/{idAksi}', 'ManageAksiController@subAksi');
$router->get('manage/detail-subaksi/{idAksi}/{idSubAksi}', 'ManageAksiController@subAksiDetail');
$router->get('manage/detail/donasi/{idAksi}/{idSubAksi}', 'ManageAksiController@detailDonasi');
$router->get('manage/detail/volunteer/{idAksi}/{idSubAksi}', 'ManageAksiController@detailVolunteer');
$router->get('manage/grup/{idUser}', 'GroupController@listGrupCreated');
$router->get('manage/donasi/{idUser}', 'KelolaController@getUserDonasi');
$router->get('manage/relawan/{idUser}', 'KelolaController@getUserRelawan');
$router->get('/riwayat-acara/{idUser}', 'KelolaController@getRiwayatAcara');
$router->post('manage/create-report', 'ManageAksiController@insertReport');
$router->post('manage/create-report-transaction', 'ManageAksiController@insertReportTransaction');
$router->post('manage/pencairan', 'ManageAksiController@pencairan');
$router->get('manage/history-pencairan/{idAksi}/{idSubAksi}', 'ManageAksiController@getHistoryPencairan');
$router->get('manage/list-acara/{idUser}', 'ManageEventController@listAcara');
$router->get('/manage/event/{idEvent}/aksi', 'ManageEventController@getAksiTerkait');

$router->post('event/insert', 'EventController@createEvent');
$router->post('event/insertSubAksi', 'EventController@createEventSubAksi');
$router->get('event/top/{idUser}', 'EventController@topEvent');
$router->get('event/new/{idUser}', 'EventController@newEvent');
$router->get('event/highlight/{idUser}', 'EventController@newEvent');
$router->post('event/new/search', 'EventController@newEventSearch');
$router->post('event/top/search', 'EventController@topEventSearch');
$router->post('event/highlight/search', 'EventController@topEventSearch');
$router->get('event/detail/{idEvent}/{idUser}', 'DetailEventController@index');
$router->post('event/join', 'DetailEventController@join');
$router->post('event/unjoin', 'DetailEventController@unjoin');
$router->get('event/list-peserta/{idEvent}', 'DetailEventController@listPeserta');
$router->get('event/aksi-terkait/{idEvent}', 'DetailEventController@aksiTerkait');
$router->get('event/cek-idevent/{idaksi}', 'DetailEventController@cekIdEvent');
$router->post('event/update', 'DetailEventController@updateEvent');
$router->get('event/peringkat/{idEvent}', 'DetailEventController@peringkat');
$router->get('event/list-invite/{idEvent}/{idUser}', 'DetailEventController@inviteList');
$router->post('event/invite/{idEvent}', 'DetailEventController@invite');

$router->post('payment-gateway-testing', 'PaymentGatewayController@test');
$router->post('payment-success', 'PaymentGatewayController@success');

$router->get('alumni/strata', 'AlumniController@getStrata');
$router->get('alumni/jurusan', 'AlumniController@getJurusan');
$router->post('alumni/user', 'AlumniController@getUser');

$router->post('version/cek', 'VersioningController@cekVersion');

$router->get('/testing/email', 'PaymentGatewayController@testingemail');
$router->get('/testinguser/{idAksi}', 'ManageAksiController@getListUser');
# PAYMENT ROUTES

$router->group(['prefix' => 'payment'], function() use ($router) {
    // Endpoint untuk membuat transaksi / donasi
    $router->post('donate', 'PaymentController@donate');
    // Endpoint untuk menangani notifikasi dari midtrans
    $router->post('notification', 'PaymentController@notification');
});

$router->group(['prefix' => 'admin'], function() use ($router) {
    $router->get('anggota/list', 'AdminController@listAnggota');
    $router->post('anggota/search', 'AdminController@searchAnggota');
    $router->get('anggota/block/{idUser}', 'AdminController@blockAnggota');
    $router->get('anggota/aktif/{idUser}', 'AdminController@aktifAnggota');

    $router->get('pencairan/list', 'AdminController@listPengajuan');
    $router->post('pencairan/proses', 'AdminController@prosesPengajuan');
    $router->post('pencairan/cair/{cair}', 'AdminController@pencairan');
    $router->post('pencairan/detail', 'AdminController@detailPengajuan');
    $router->post('pencairan/search', 'AdminController@searchPencairan');

    $router->get('grup/list', 'AdminController@listGrup');
    $router->post('grup/search', 'AdminController@searchGrup');
    $router->get('grup/block/{idUser}', 'AdminController@blockGrup');
    $router->get('grup/aktif/{idUser}', 'AdminController@aktifGrup');

    $router->get('aksi/list', 'AdminController@listAksi');
    $router->get('aksi/blok/{idAksi}', 'AdminController@blokAksi');
    $router->get('aksi/aktif/{idAksi}', 'AdminController@aktifAksi');
    $router->post('aksi/search', 'AdminController@searchAksi');

    $router->get('acara/list', 'AdminController@listAcara');
    $router->post('acara/search', 'AdminController@searchAcara');
    $router->get('acara/blok/{idAcara}', 'AdminController@blokAcara');
    $router->get('acara/aktif/{idAcara}', 'AdminController@aktifAcara');

    $router->post('notifikasi/send', 'AdminController@sendNotifikasi');
});