<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class AksiReportFoto extends Model
{
    protected $table = 'aksi_report_foto';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idreport',
        'idfoto',
        'urlfotoreport'
    ];
}