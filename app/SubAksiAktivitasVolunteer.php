<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SubAksiAktivitasVolunteer extends Model
{
    protected $table = 'sub_aksi_aktivitas_volunteer';
    public $timestamps = false;

    protected $hidden = [

    ];

    protected $fillable = [
        'idaksi',
        'idsubaksi',
        'idvolunteer',
        'iduser',
        'idaktivitas'
    ];
}