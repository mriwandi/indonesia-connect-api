<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class GrupStatus extends Model
{
    protected $table = 'grup_status';
    protected $primaryKey = 'idgrup';
    public $timestamps = false;

    protected $fillable = [
        'idgrup',
        'status'
    ];
}