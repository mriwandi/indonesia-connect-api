<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class AksiFoto extends Model
{
    use Compoships;

    protected $table = 'aksi_foto';
    public $timestamps = false;

    protected $fillable = [
        'idaksi', 'idfoto', 'urlfotoaksi'
    ];

    public function aksi(){
        return $this->belongsTo('App\Aksi','idaksi');
    }
}