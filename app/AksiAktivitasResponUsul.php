<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class AksiAktivitasResponUsul extends Model
{
    use Compoships;

    protected $table = 'aksi_aktivitas_respon_usul';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idusul',
        'idrespon',
        'iduser',
        'idaktivitas'
    ];

    public function userAktivitasUsul(){
        return $this->hasOne('App\UserAktivitasUsul', ['iduser', 'idaktivitas'], ['iduser', 'idaktivitas']);
    }

    public function user(){
        return $this->hasOne('App\User', ['iduser'],['iduser']);
    }
}