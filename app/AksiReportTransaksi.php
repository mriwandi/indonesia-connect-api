<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class AksiReportTransaksi extends Model
{
    protected $table = 'aksi_report_transaksi';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idreport',
        'idtransaksi',
        'nilaitransaksi',
        'keterangantransaksi'
    ];
}