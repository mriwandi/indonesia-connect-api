<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Grup extends Model
{
    protected $table = 'grup';
    protected $primaryKey = 'idgrup';
    public $timestamps = false;

    protected $fillable = [
        'idgrup',
        'namagrup',
        'iduserinitiator',
        'keterangan'
    ];

    public function grupDetail(){
        return $this->hasOne('App\GrupDetail','idgrup');
    }

    public function grupStatus(){
        return $this->hasOne('App\GrupStatus','idgrup');
    }
}