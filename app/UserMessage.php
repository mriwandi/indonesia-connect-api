<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class UserMessage extends Model
{
    protected $table = 'user_message';
    protected $primaryKey = 'idpesan';
    public $timestamps = false;

    protected $fillable = [
        'iduserpengirim','iduserpenerima','idpesan','idroom','isipesan','tanggalkirim','waktukirim','statusbaca'
    ];
}