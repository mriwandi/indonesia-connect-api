<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class EventAksi extends Model
{
    protected $table = 'event_aksi';
    public $timestamps = false;

    protected $fillable = [
        'idevent',
        'idaksi'
    ];

    public function subaksi(){
        return $this->hasMany('App\SubAksi','idaksi','idaksi');
    }
}