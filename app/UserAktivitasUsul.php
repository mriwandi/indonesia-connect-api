<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class UserAktivitasUsul extends Model
{
    use Compoships;

    protected $table = 'user_aktivitas_usul';
    public $timestamps = false;

    protected $fillable = [
        'iduser',
        'idaktivitas',
        'isi'
    ];

    public function userAktivitas(){
        return $this->hasOne('App\UserAktivitas',['iduser', 'idaktivitas'], ['iduser', 'idaktivitas']);
    }

    public function aksiAktivitasResponUsul(){
        return $this->hasOne('App\AksiAktivitasResponUsul', ['iduser', 'idaktivitas'], ['iduser', 'idaktivitas']);
    }
}