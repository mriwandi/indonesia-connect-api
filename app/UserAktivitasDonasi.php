<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class UserAktivitasDonasi extends Model
{
    use Compoships;

    protected $table = 'user_aktivitas_donasi';
    public $timestamps = false;

    protected $fillable = [
        'iduser',
        'idaktivitas',
        'nilaidonasi',
        'keterangan'
    ];
}