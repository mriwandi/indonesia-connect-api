<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class SubAksiVolunteer extends Model
{
    use Compoships;

    protected $table = 'sub_aksi_volunteer';
    protected $primaryKey = 'id_subaksi';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idsubaksi',
        'tanggalmulai',
        'tanggalselesai',
        'tanggalmulaiaksi',
        'tanggalselesaiaksi',
        'waktumulaiaksi',
        'waktuselesaiaksi',
        'targetvolunteer'
    ];
}