<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class AksiAktivitasUsul extends Model
{
    use Compoships;

    protected $table = 'aksi_aktivitas_usul';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idusul',
        'iduser',
        'idaktivitas'
    ];

    public function userAktivitasUsul(){
        return $this->hasOne('App\UserAktivitasUsul', ['iduser','idaktivitas'],['iduser', 'idaktivitas']);
    }
}