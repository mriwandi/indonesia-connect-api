<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SubGrup extends Model
{
    protected $table = 'sub_grup';
    protected $primaryKey = 'idsubgrup';
    public $timestamps = false;

    protected $fillable = [
        'idgrup',
        'idsubgrup',
        'namasubgrup',
        'iduserinitiator',
        'keterangan'
    ];
}