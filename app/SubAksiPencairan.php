<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SubAksiPencairan extends Model
{
    protected $table = 'sub_aksi_pencairan';
    public $timestamps = false;

    protected $fillable = [
      'idaksi',
      'idsubaksi',
      'idpencairan',
      'tanggal',
      'waktu',
      'nominal',
      'rekening',
      'namabank',
      'namapemilikrekening',
      'statuspencairan'
    ];

    public function aksi(){
        return $this->hasOne('App\Aksi', 'idaksi', 'idaksi');
    }
}