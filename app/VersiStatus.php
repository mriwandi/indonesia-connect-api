<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class VersiStatus extends Model
{
    protected $table = 'versi_status';
    public $timestamps = false;

    protected $fillable = [
        'versi','keterangan'
    ];
}