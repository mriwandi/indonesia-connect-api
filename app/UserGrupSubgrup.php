<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class UserGrupSubgrup extends Model
{
    protected $table = 'user_grup_subgrup';
    public $timestamps = false;

    protected $hidden = [

    ];

    protected $fillable = [
        'iduser',
        'idgrup',
        'idsubgrup'
    ];

    public function user(){
        return $this->belongsTo('App\User','iduser','iduser');
    }

    public function grup(){
        return $this->belongsTo('App\Grup','idgrup','idgrup');
    }

    public function subGrup(){
        return $this->belongsTo('App\SubGrup','idsubgrup','idsubgrup');
    }
}