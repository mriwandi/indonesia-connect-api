<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class UserAktivitasEvent extends Model
{
    protected $table = 'user_aktivitas_event';
    public $timestamps = false;

    protected $fillable = [
        'iduser',
        'idaktivitas',
        'idevent',
        'idjenisaktivitas',
        'tanggalaktivitas',
        'waktuaktivitas',
        'keterangan'
    ];

    public function event(){
        return $this->hasOne('App\Event','idevent','idevent');
    }

    public function user(){
        return $this->hasOne('App\User','iduser','iduser');
    }

    public function jenisAktivitas(){
        return $this->hasOne('App\JenisAktivitas','idjenisaktivitas','idjenisaktivitas');
    }
}