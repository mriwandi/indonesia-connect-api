<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class EventJoin extends Model
{
    protected $table = 'event_join';
    public $timestamps = false;

    protected $fillable = [
        'idevent',
        'iduser_join',
        'tanggaljoin',
        'waktujoin'
    ];

    public function user(){
        return $this->hasOne('App\User','iduser','iduser_join');
    }
}