<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class Aksi extends Model
{
    use Compoships;

    protected $table = 'aksi';
    protected $primaryKey = 'idaksi';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'iduser_initiator',
        'idgrup',
        'idsubgrup',
        'namaaksi',
        'deskripsi',
        'tanggalpembuatan',
        'waktupembuatan',
        'status',
        'tipeaksi'
    ];

    public function aksiFoto(){
        return $this->hasOne('App\AksiFoto', 'idaksi');
    }

    public function subAksi(){
        return $this->hasMany('App\SubAksi','idaksi');
    }

    public function user(){
        return $this->belongsTo('App\User','iduser_initiator','iduser');
    }

}