<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class AksiReport extends Model
{
    protected $table = 'aksi_report';
    protected $primaryKey = 'idreport';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idreport',
        'tanggalreport',
        'waktureport'
    ];

    public function deskriptif(){
        return $this->hasOne('App\AksiReportDeskriptif', 'idreport');
    }

    public function transaksi(){
        return $this->hasMany('App\AksiReportTransaksi','idreport');
    }

    public function foto(){
        return $this->hasMany('App\AksiReportFoto','idreport');
    }
}