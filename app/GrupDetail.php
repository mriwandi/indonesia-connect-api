<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class GrupDetail extends Model
{
    protected $table = 'grup_detail';
    protected $primaryKey = 'idgrup';
    public $timestamps = false;

    protected $fillable = [
        'idgrup',
        'urlfoto',
        'deskripsi'
    ];
}