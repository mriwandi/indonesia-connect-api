<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class UserAktivitasVolunteer extends Model
{
    use Compoships;

    protected $table = 'user_aktivitas_volunteer';
    public $timestamps = false;

    protected $fillable = [
        'iduser',
        'idaktivitas',
        'tanggalawalkesediaan',
        'tanggalakhirkesediaan',
        'waktuawalkesediaan',
        'waktuakhirkesediaan',
        'handphone'
    ];
}