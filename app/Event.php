<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Event extends Model
{
    protected $table = 'event';
    protected $primaryKey = 'idevent';
    public $timestamps = false;

    protected $fillable = [
        'idevent',
        'iduser_initiator',
        'idgrup',
        'idsubgrup',
        'namaevent',
        'deskripsi',
        'tanggalawalevent',
        'waktuawalevent',
        'tanggalakhirevent',
        'waktuakhirevent',
        'lokasievent',
        'latitude',
        'longitude',
        'tanggalpembuatan',
        'waktupembuatan',
        'status'
    ];

    public function eventFoto(){
        return $this->hasMany('App\EventFoto', 'idevent');
    }
}