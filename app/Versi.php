<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Versi extends Model
{
    protected $table = 'versi';
    public $timestamps = false;

    protected $fillable = [
        'idversi','status'
    ];
}