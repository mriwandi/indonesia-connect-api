<?php
    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\UserAktivitas;
    use Illuminate\Support\Facades\DB;

    class KelolaController extends Controller
    {
        public function getUserDonasi($idUser){
            $data = UserAktivitas::where('idjenisaktivitas',2)->where('iduser', $idUser)->with('userAktivitasDonasi')->with('Aksi')->orderBy('idaktivitas', 'desc')->get();

            $res['success'] = true;
            $res['message'] = "Data User Tersedia";
            $res['data'] = $data;
            return response($res,200);
        }

        public function getUserRelawan($idUser){
            $data = UserAktivitas::where('idjenisaktivitas',3)->where('iduser', $idUser)->with('userAktivitasVolunteer')->with('Aksi')->orderBy('idaktivitas', 'desc')->get();

            $res['success'] = true;
            $res['message'] = "Data Relawan tersedia";
            $res['data'] = $data;
            return response($res,200);
        }

        public function getRiwayatAcara($idUser){
            $query = "select idevent, tanggaljoin, waktujoin, iduser_initiator, namaevent, tanggalawalevent, waktuawalevent, tanggalakhirevent, waktuakhirevent, lokasievent
                    from event_join inner join event using(idevent) where iduser_join = $idUser order by tanggaljoin, waktujoin desc";
            $data = DB::select(DB::raw($query));

            return response($data,200);
        }
    }