<?php
namespace App\Http\Controllers;
use App\Event;
use App\EventAksi;
use Illuminate\Http\Request;
use App\Aksi;
use App\AksiDetail;
use App\SubAksi;
use App\SubAksiDonasi;
use App\SubAksiVolunteer;
use App\UserAktivitas;
use App\AksiReport;
use App\AksiReportDeskriptif;
use App\AksiReportFoto;
use App\AksiReportTransaksi;
use App\SubAksiPencairan;

use Illuminate\Support\Facades\DB;
class ManageAksiController extends Controller
{
    public function listAksi($idUser){
        $query = Aksi::where('iduser_initiator', $idUser)->where('tipeaksi',2)->with('subAksi')->orderBy('namaaksi', 'asc')->get();
        $i = 0;
        foreach ($query as $q){
            $query[$i]['testing'] = $q->idaksi;
            $today = date('Y-m-d');
            $today =date('Y-m-d', strtotime($today));
            if($q->tipeaksi == '2'){
                $donasi = SubAksiDonasi::where('idaksi', $q->idaksi)->where('idsubaksi', 0)->first();
                if($donasi){
                    $donasiMulai = date('Y-m-d', strtotime($donasi->tanggalmulai));
                    $donasiSelesai = date('Y-m-d', strtotime($donasi->tanggalselesai));

                    if (($today > $donasiMulai) && ($today < $donasiSelesai)){
                        $query[$i]['status_donasi'] = 'Sedang Berjalan';
                    }else if($today > $donasiSelesai){
                        $query[$i]['status_donasi'] = 'Telah Berakhir';
                    }else if($today < $donasiMulai){
                        $query[$i]['status_donasi'] = 'Belum dimulai';
                    }
                }

                $relawan = SubAksiVolunteer::where('idaksi', $q->idaksi)->where('idsubaksi', 0)->first();
                if($relawan){
                    $relawanMulai = date('Y-m-d', strtotime($relawan->tanggalmulai));
                    $relawanSelesai = date('Y-m-d', strtotime($relawan->tanggalselesai));

                    if (($today > $relawanMulai) && ($today < $relawanSelesai)){
                        $query[$i]['status_relawan'] = 'Sedang Berjalan';
                    }else if($today > $relawanSelesai){
                        $query[$i]['status_relawan'] = 'Telah Berakhir';
                    }else if($today < $relawanMulai){
                        $query[$i]['status_relawan'] = 'Belum dimulai';
                    }
                }
            }else{
                $eventId = EventAksi::where('idaksi', $q->idaksi)->first()->idevent;
                $event = Event::where('idevent', $eventId)->first();
                if($event){
                    $eventMulai = date('Y-m-d', strtotime($event->tanggalawalevent));
                    $eventSelesai = date('Y-m-d', strtotime($event->tanggalakhirevent));

                    if (($today > $eventMulai) && ($today < $eventSelesai)){
                        $query[$i]['status_event'] = 'Sedang Berjalan';
                    }else if($today > $eventSelesai){
                        $query[$i]['status_event'] = 'Telah Berakhir';
                    }else if($today < $eventMulai){
                        $query[$i]['status_event'] = 'Belum dimulai';
                    }
                }
            }
            $i++;
        }
        $res['success'] = true;
        $res['message'] = "Your Action List";
        $res['data']    = $query;

        return response($res, 200);
    }

    public function subAksi($idAksi){
        $query = Aksi::where('idaksi', $idAksi)->with('subAksi')->first();

        $res['success'] = true;
        $res['message'] = "Your Sub Aksi List";
        $res['data']    = $query;

        return response($res, 200);
    }

    private function getSubAksiDonasi($idAksi, $idSubAksi){
        $query = SubAksiDonasi::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->first();
        if($query == null){
            return null;
        }else{
            return $query;
        }
    }

    private function getSubAksiVolunteer($idAksi, $idSubAksi){
        $query = SubAksiVolunteer::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->first();
        if($query == null){
            return null;
        }else{
            return $query;
        }
    }

    private function getTotalDonatur($idAksi, $idSubAksi){
        $query = UserAktivitas::where('idaksi',$idAksi)->where('idsubaksi',$idSubAksi)->where('idjenisaktivitas', 2)->get();
        return count($query);
    }

    private function getTotalVolunteer($idAksi, $idSubAksi){
        $query = UserAktivitas::where('idaksi',$idAksi)->where('idsubaksi', $idSubAksi)->where('idjenisaktivitas', 3)->get();
        return count($query);
    }

    private function getTotalDonasi($idAksi, $idSubAksi){
        $jumlahDonasi = DB::select(DB::raw("SELECT SUM(nilaidonasi) AS total FROM user_aktivitas_donasi INNER JOIN sub_aksi_aktivitas_donasi USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$idSubAksi."'"));
        if($jumlahDonasi[0]->total != null)
            return $jumlahDonasi[0]->total;
        else
            return 0;
    }

    private function getTotalPencairan($idAksi, $idSubAksi){
        $query = DB::select("SELECT SUM(nominal) AS total FROM sub_aksi_pencairan WHERE idaksi='".$idAksi."' AND idsubaksi='".$idSubAksi."'");
        $total = (int)$query[0]->total;
        if($total != null){
            return $total;
        }else{
            return 0;
        }

    }

    public function subAksiDetail($idAksi, $idSubAksi){
        $query = SubAksi::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->with('aksi.aksiFoto')->first();
        $data = $query;
        $data['donasi'] = $this->getSubAksiDonasi($idAksi, $idSubAksi);
        $data['volunteer'] = $this->getSubAksiVolunteer($idAksi, $idSubAksi);
        $data['totaldonasi'] = $this->getTotalDonasi($idAksi, $idSubAksi);
        $data['totaldonatur'] = $this->getTotalDonatur($idAksi, $idSubAksi);
        $data['totalvolunteer'] = $this->getTotalVolunteer($idAksi, $idSubAksi);
        $data['totalpencairan'] = $this->getTotalPencairan($idAksi, $idSubAksi);
        $data['sisapencairan'] = $data['totaldonasi'] - $data['totalpencairan'];
        $res['success'] = true;
        $res['message'] = "Your Sub Aksi List";
        $res['data']    = $data;

        return response($res, 200);
    }

    public function detailDonasi($idAksi, $idSubAksi){
        $query = UserAktivitas::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->where('idjenisaktivitas', 2)->with('userAktivitasDonasi')->with('user')->orderBy('tanggalaktivitas', 'asc')->orderBy('waktuaktivitas', 'asc')->get();
        $data = $query;
        $res['success'] = true;
        $res['message'] = "Detail Donasi List";
        $res['data']    = $data;

        return response($res, 200);
    }

    public function detailVolunteer($idAksi, $idSubAksi){
        $query = UserAktivitas::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->where('idjenisaktivitas', 3)->with('userAktivitasVolunteer')->with('user')->orderBy('tanggalaktivitas', 'asc')->orderBy('waktuaktivitas', 'asc')->get();
        $data = $query;
        $res['success'] = true;
        $res['message'] = "Detail Volunteer List";
        $res['data']    = $data;

        return response($res, 200);
    }

    public function getIdReport($idAksi){
        return count(AksiReport::where('idaksi', $idAksi)->get()) + 1;
    }

    public function getIdFoto($idReport, $idAksi){
        return count(AksiReportFoto::where('idaksi', $idAksi)->where('idreport', $idReport)->get()) + 1;
    }

    public function getIdTransaksi($idReport, $idAksi){
        return count(AksiReportTransaksi::where('idaksi', $idAksi)->where('idreport', $idReport)->get()) + 1;
    }


    public function insertReportTransaction(Request $request){
        $data                   = $request->all();
        $data['idreport']       = $this->getIdReport($data['idaksi']);
        $data['idfoto']         = $this->getIdFoto($data['idreport'], $data['idaksi']);
        $data['tanggalreport']  = date("Y-m-d");
        $data['waktureport']    = date("h:i:s");
        $transaction            = $request->transaksi;

        AksiReport::create([
            'idaksi'            => $data['idaksi'],
            'idreport'          => $data['idreport'],
            'tanggalreport'     => $data['tanggalreport'],
            'waktureport'       => $data['waktureport']
        ]);

        $i=1;
        foreach($transaction as $t){
            AksiReportTransaksi::create([
                'idaksi'                => $data['idaksi'],
                'idreport'              => $data['idreport'],
                'idtransaksi'           => $i,
                'nilaitransaksi'        => $t['jumlah'],
                'keterangantransaksi'   => $t['keterangan']
            ]);
            $i++;
        }

        $list = [];
        $users = UserAktivitas::select('iduser')->where('idaksi', $data['idaksi'])->where('idjenisaktivitas', 2)->orWhere('idjenisaktivitas', 3)->get();
        $namaAksi = Aksi::where('idaksi', $data['idaksi'])->first()->namaaksi;
        foreach($users as $user){
            array_push($list, $user->iduser);
        }
        foreach(array_unique($list) as $push){
            // \OneSignal::sendNotificationUsingTags(
            //     "Terdapat Laporan Baru Untuk Aksi $namaAksi",
            //     array(
            //         ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $push]
            //     ),
            //     $url = null,
            //     $data = null,
            //     $buttons = null,
            //     $schedule = null
            // );
        }


        $res['success'] = true;
        $res['data']    = $data;
        return response($res, 200);
    }

    public function getListUser($idAksi){
        $list = [];
        $users = UserAktivitas::select('iduser')->where('idaksi', $idAksi)->where('idjenisaktivitas', 2)->orWhere('idjenisaktivitas', 3)->get();
        foreach($users as $user){
            array_push($list, $user->iduser);
        }
        foreach(array_unique($list) as $push){
            echo $push;
        }
    }

    public function insertReport(Request $request){
        $data                   = $request->all();
        $data['idreport']       = $this->getIdReport($data['idaksi']);
        $data['idfoto']         = $this->getIdFoto($data['idreport'], $data['idaksi']);
        $data['tanggalreport']  = date("Y-m-d");
        $data['waktureport']    = date("h:i:s");

        AksiReport::create([
            'idaksi'            => $data['idaksi'],
            'idreport'          => $data['idreport'],
            'tanggalreport'     => $data['tanggalreport'],
            'waktureport'       => $data['waktureport']
        ]);

        AksiReportDeskriptif::create([
            'idaksi'         => $data['idaksi'],
            'idreport'       => $data['idreport'],
            'deskripsi'      => $data['deskripsi']
        ]);

        if($request->hasFile('urlfotoreport')){
            $image = $request->file('urlfotoreport');
            $filename = "report". $data['idreport'] . '.'.$image->getClientOriginalExtension();
            $image->move(public_path("/upload/report/"), $filename);
            $url = url('/') . '/upload/report/' . $filename;
            $urlfoto = $url;
        }else{
            $urlfoto = null;
        }

        if($request->urlfoto != ''){
            $data['urlfotoreport'] = 'http://apis.ganeshaconnect.com/aksi/report/'.$this->base64_to_jpeg($request->urlfoto, $data['idaksi'] .'-'. $data['idreport'] . '(' . $data['idfoto'] . ').jpg');

            AksiReportFoto::create([
                'idaksi'        => $data['idaksi'],
                'idreport'      => $data['idreport'],
                'idfoto'        => $data['idfoto'],
                'urlfotoreport' => $data['urlfotoreport']
            ]);
        }

        $list = [];
        $users = UserAktivitas::select('iduser')->where('idaksi', $data['idaksi'])->where('idjenisaktivitas', 2)->orWhere('idjenisaktivitas', 3)->get();
        $namaAksi = Aksi::where('idaksi', $data['idaksi'])->first()->namaaksi;
        foreach($users as $user){
            array_push($list, $user->iduser);
        }
        foreach(array_unique($list) as $push){
            // \OneSignal::sendNotificationUsingTags(
            //     "Terdapat Laporan Baru Untuk Aksi $namaAksi",
            //     array(
            //         ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $push]
            //     ),
            //     $url = null,
            //     $data = null,
            //     $buttons = null,
            //     $schedule = null
            // );
        }


        $res['success'] = true;
        $res['data']    = $data;
    }

    private function getIdPencairan($idAksi, $idSubAksi) {
        return SubAksiPencairan::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->count();
    }

    public function pencairan(Request $request){
        $tanggal        = date("Y-m-d");
        $waktu          = date("h:i:s");
        $idPencairan    = $this->getIdPencairan($request->idaksi, $request->idsubaksi);

        SubAksiPencairan::create([
            'idaksi'                => $request->idaksi,
            'idsubaksi'             => $request->idsubaksi,
            'idpencairan'           => $idPencairan,
            'tanggal'               => $tanggal,
            'waktu'                 => $waktu,
            'nominal'               => $request->nominal,
            'rekening'              => $request->rekening,
            'namabank'              => $request->namabank,
            'namapemilikrekening'   => $request->namapemilikrekening,
            'statuspencairan'       => 1
        ]);

        $data = $request->all();


        $res['success'] = true;
        $res['message'] = "Pencairan berhasil, silahkan menunggu beberapa saat.";
        $res['data']    = $data;

        return response($res, 200);
    }

    public function getHistoryPencairan($idAksi, $idSubAksi){
        $data = SubAksiPencairan::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->orderBy('idpencairan', 'desc')->get();

        $res['success'] = true;
        $res['message'] = "List Pencairan";
        $res['data']    = $data;

        return response($res, 200);
    }
}