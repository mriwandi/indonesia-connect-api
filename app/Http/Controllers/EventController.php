<?php
    namespace App\Http\Controllers;
    use App\EventAksi;
    use App\EventJoin;
    use App\SubAksiFoto;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use App\Event;
    use App\EventFoto;
    use App\UserAktivitasEvent;
    use App\Aksi;
    use App\AksiFoto;
    use App\SubAksi;
    use App\SubAksiDonasi;
    use App\SubAksiVolunteer;
    use App\UserAktivitas;

    class EventController extends Controller
    {
        private function getEventId(){
            return count(Event::all()) + 1;
        }

        private function getFotoId($idevent){
            return count(EventFoto::where('idevent', $idevent)->get()) + 1;
        }

        private function getFotoSubAksiId($idAksi, $idSubAksi){
            return count(SubAksiFoto::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->get()) + 1;
        }

        private function base64_to_jpeg($base64_string, $output_file, $folderImg) {
            // open the output file for writing
            $ifp = fopen( $folderImg.$output_file, 'wb' );

            // split the string on commas
            // $data[ 0 ] == "data:image/png;base64"
            // $data[ 1 ] == <actual base64 string>
            $data = explode( ',', $base64_string );

            // we could add validation here with ensuring count( $data ) > 1
            fwrite( $ifp, base64_decode( $base64_string ) );

            // clean up the file resource
            fclose( $ifp );

            return $output_file;
        }

        protected function getAktivitasEventId($id){
            return count(UserAktivitasEvent::where('iduser', $id)->get()) + 1;
        }

        protected function getAktivitasId($id){
            return count(UserAktivitas::where('iduser', $id)->get()) + 1;
        }

        protected function getAksiId(){
            return count(Aksi::all()) + 1;
        }

        protected function getSubAksiId($idAksi){
            return count(SubAksi::where('idaksi', $idAksi)->get()) + 1;
        }

        public function createEvent(Request $request){
            $data                       = $request->all();
            $data['idevent']            = $this->getEventId();
            $data['aktivitas']          = $this->getAktivitasId($data['iduser']);
            $data['idaktivitas']        = $this->getAktivitasEventId($data['iduser']);
            $data['idfoto']             = $this->getFotoId($data['idevent']);

            $folderImg                  = 'eventImg/';

            if($request->hasFile('urlfotoevent')){
                $image = $request->file('urlfotoevent');
                $filename = "event". $data['idevent'] . '.'.$image->getClientOriginalExtension();
                $done = $image->move(public_path("/upload/event/"), $filename);
                $url = url('/') . '/upload/event/' . $filename;
                $data['urlfotoevent'] = $url;
            }else{
                $data['urlfotoevent'] = null;
            }

            $data['idjenisaktivitas']   = 7;
            $data['tanggalpembuatan']   = date("Y-m-d");
            $data['waktupembuatan']     = date("h:i:s");
            $data['keterangan']         = "Membuat event baru";

            $data['idaksi']             = $this->getAksiId();
            $data['tipeaksi']           = 1;

            $this->insertEvent($data);
            $data['urlfoto'] = "uploaded";
            $res['success'] = true;
            $res['data']    = $data;

            return response($res,200);
        }

        public function insertEvent($data){
            Event::create([
                'idevent'               => $data['idevent'],
                'iduser_initiator'      => $data['iduser'],
                'idgrup'                => $data['idgrup'],
                'idsubgrup'             => $data['idsubgrup'],
                'namaevent'             => $data['namaevent'],
                'deskripsi'             => $data['deskripsi'],
                'tanggalawalevent'      => $data['tanggalawal'],
                'waktuawalevent'        => $data['waktuawal'],
                'tanggalakhirevent'     => $data['tanggalakhir'],
                'waktuakhirevent'       => $data['waktuakhir'],
                'lokasievent'           => $data['lokasi'],
                'latitude'              => $data['lat'],
                'longitude'             => $data['long'],
                'tanggalpembuatan'      => $data['tanggalpembuatan'],
                'waktupembuatan'        => $data['waktupembuatan'],
                'status'                => 1
            ]);

            EventFoto::create([
                'idevent'       => $data['idevent'],
                'idfoto'        => $data['idfoto'],
                'urlfotoevent'  => $data['urlfotoevent']
            ]);

            EventJoin::create([
                'idevent'       => $data['idevent'],
                'iduser_join'   => $data['iduser'],
                'tanggaljoin'   => $data['tanggalpembuatan'],
                'waktujoin'     => $data['waktupembuatan']
            ]);

            UserAktivitasEvent::create([
                'iduser'            => $data['iduser'],
                'idaktivitas'       => $data['idaktivitas'],
                'idevent'           => $data['idevent'],
                'idjenisaktivitas'  => $data['idjenisaktivitas'],
                'tanggalaktivitas'  => $data['tanggalpembuatan'],
                'waktuaktivitas'    => $data['waktupembuatan'],
                'keterangan'        => $data['keterangan']
            ]);

            if($data['setAksi'] == true){
                Aksi::create([
                    'idaksi'            => $data['idaksi'],
                    'iduser_initiator'  => $data['iduser'],
                    'idgrup'            => $data['idgrup'],
                    'idsubgrup'         => $data['idsubgrup'],
                    'namaaksi'          => $data['namaevent'],
                    'deskripsi'         => $data['deskripsi'],
                    'tanggalpembuatan'  => $data['tanggalpembuatan'],
                    'waktupembuatan'    => $data['waktupembuatan'],
                    'status'            => 1,
                    'tipeaksi'          => $data['tipeaksi']
                ]);

                AksiFoto::create([
                    'idaksi'            => $data['idaksi'],
                    'idfoto'            => $data['idfoto'],
                    'urlfotoaksi'       => $data['urlfotoevent']
                ]);

                EventAksi::create([
                    'idevent'   => $data['idevent'],
                    'idaksi'    => $data['idaksi']
                ]);
            }
        }

        public function createEventSubAksi(Request $request){
            $data                       = $request->all();
            $data['idsubaksi']          = $this->getSubAksiId($data['idaksi']);
            $data['idaktivitas']        = $this->getAktivitasId($data['iduser_initiator']);
            $data['idfoto']             = $this->getFotoSubAksiId($data['idaksi'], $data['idsubaksi']);
            $data['tipeaksi']           = '1';

            $folderImg                  = 'aksi/';


            if(!$request->urlfoto){
                $data['urlfoto'] = null;
            }else{
                $data['urlfoto'] = 'http://apis.ganeshaconnect.com/'. $folderImg .$this->base64_to_jpeg($request->urlfoto, $data['idaksi'] .'-'. $data['idsubaksi'] .'-'. $data['idfoto'] . '.jpg', $folderImg);
            }

            $data['idjenisaktivitas']   = 11;
            $data['tanggalpembuatan']   = date("Y-m-d");
            $data['waktupembuatan']     = date("h:i:s");
            $data['keterangan']         = "Membuat Aksi untuk Acara";
            $data['status']             = "1";

            $this->insertEventSubAksi($data);
            $res['success'] = true;
            $res['data']    = $data;

            return response($res,200);
        }

        public function insertEventSubAksi($data){
            SubAksi::create([
                'idaksi'            => $data['idaksi'],
                'idsubaksi'         => $data['idsubaksi'],
                'idjenisaksi'       => $data['idjenisaksi'],
                'namasubaksi'       => $data['namasubaksi'],
                'deskripsisubaksi'  => $data['deskripsisubaksi'],
                'tanggalpembuatan'  => $data['tanggalpembuatan'],
                'waktupembuatan'    => $data['waktupembuatan'],
                'status'            => $data['status']
            ]);

            SubAksiFoto::create([
                'idaksi'            => $data['idaksi'],
                'idsubaksi'         => $data['idsubaksi'],
                'idfoto'            => $data['idfoto'],
                'urlfotosubaksi'    => $data['urlfoto']
            ]);

            if($data['donasi']['status']){
                SubAksiDonasi::create([
                    'idaksi'            => $data['idaksi'],
                    'idsubaksi'         => $data['idsubaksi'],
                    'tanggalmulai'      => $data['donasi']['tanggalmulai'],
                    'tanggalselesai'    => $data['donasi']['tanggalselesai'],
                    'targetdonasi'      => $data['donasi']['targetdonasi']
                ]);
            }

            if($data['volunteer']['status']){
                SubAksiVolunteer::create([
                    'idaksi'                => $data['idaksi'],
                    'idsubaksi'             => $data['idsubaksi'],
                    'tanggalmulai'          => $data['volunteer']['tanggalmulai'],
                    'tanggalselesai'        => $data['volunteer']['tanggalselesai'],
                    'tanggalmulaiaksi'      => $data['volunteer']['tanggalmulaiaksi'],
                    'tanggalselesaiaksi'    => $data['volunteer']['tanggalselesaiaksi'],
                    'waktumulaiaksi'        => $data['volunteer']['waktumulaiaksi'],
                    'waktuselesaiaksi'      => $data['volunteer']['waktuselesaiaksi'],
                    'targetvolunteer'       => $data['volunteer']['targetvolunteer']
                ]);
            }

            UserAktivitas::create([
                'iduser'                => $data['iduser_initiator'],
                'idaktivitas'           => $data['idaktivitas'],
                'idaksi'                => $data['idaksi'],
                'idsubaksi'             => $data['idsubaksi'],
                'idjenisaktivitas'      => $data['idjenisaktivitas'],
                'tanggalaktivitas'      => $data['tanggalpembuatan'],
                'waktuaktivitas'        => $data['waktupembuatan'],
                'keterangan'            => $data['keterangan']
            ]);
        }

        public function topEvent($idUser){
            $aksi = DB::select(DB::raw("SELECT idevent,namaevent,idfoto,urlfotoevent,iduser_initiator,fullname,jumlah,tanggalpembuatan,waktupembuatan, status
                FROM
                (
                    SELECT idevent,namaevent,iduser_initiator,fullname,jumlah,tanggalpembuatan,waktupembuatan, status
                    FROM
                    (
                        SELECT event.idevent AS idevent,namaevent,iduser_initiator,jumlah,tanggalpembuatan,waktupembuatan, status
                
                        FROM `event`
                
                        INNER JOIN
                        (
                            SELECT idevent,count(*) AS jumlah FROM `user_aktivitas_event`
                            WHERE 
                
                            idevent IN 
                            (
                                SELECT idevent FROM event 
                                WHERE 
                                (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                )
                
                                OR 
                                (
                                    idgrup IN 
                                    (
                                        SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                    )
                                    AND idsubgrup='0'
                                )
                
                                OR (idgrup='0' AND idsubgrup='0')
                            )
                
                            GROUP BY idevent
                        ) AS proses
                
                        WHERE event.idevent = proses.idevent AND event.status = 1
                
                        ORDER BY jumlah DESC
                    ) AS proses2
                
                    INNER JOIN user
                
                    WHERE user.iduser = proses2.iduser_initiator
                ) AS proses3
                
                INNER JOIN
                
                event_foto USING (idevent)"));

            $res['success'] = true;
            $res['data'] = $aksi;
            return response($res,200);
        }

        public function topEventSearch(Request $request){
            $idUser = $request->iduser;
            $search = $request->search;
            $aksi = DB::select(DB::raw("SELECT idevent,namaevent,idfoto,urlfotoevent,iduser_initiator,fullname,jumlah,tanggalpembuatan,waktupembuatan, status
                FROM
                (
                    SELECT idevent,namaevent,iduser_initiator,fullname,jumlah,tanggalpembuatan,waktupembuatan, status
                    FROM
                    (
                        SELECT event.idevent AS idevent,namaevent,iduser_initiator,jumlah,tanggalpembuatan,waktupembuatan, status
                
                        FROM `event`
                
                        INNER JOIN
                        (
                            SELECT idevent,count(*) AS jumlah FROM `user_aktivitas_event`
                            WHERE 
                
                            idevent IN 
                            (
                                SELECT idevent FROM event 
                                WHERE 
                                (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                )
                
                                OR 
                                (
                                    idgrup IN 
                                    (
                                        SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                    )
                                    AND idsubgrup='0'
                                )
                
                                OR (idgrup='0' AND idsubgrup='0')
                            )
                
                            GROUP BY idevent
                        ) AS proses
                
                        WHERE event.idevent = proses.idevent AND event.status = 1
                
                        ORDER BY jumlah DESC
                    ) AS proses2
                
                    INNER JOIN user
                
                    WHERE user.iduser = proses2.iduser_initiator
                ) AS proses3
                
                INNER JOIN
                
                event_foto USING (idevent) WHERE proses3.namaevent LIKE '%$search%'"));

            $res['success'] = true;
            $res['data'] = $aksi;
            return response($res,200);
        }

        public function newEvent($idUser){
            $i = 0;
            $aksi = DB::select(
                DB::raw("SELECT idevent,namaevent,idfoto,urlfotoevent,tanggalpembuatan,waktupembuatan,iduser_initiator,fullname, status FROM
                        (
                            SELECT idevent,namaevent,tanggalpembuatan,waktupembuatan,iduser_initiator,status FROM event
                            WHERE
                            (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                )
                                OR 
                                (
                                    idgrup IN
                                    (
                                      SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                    )
                                    AND idsubgrup='0'
                                )
                            OR (idgrup='0' AND idsubgrup='0')
                        ) AS proses
                        
                        INNER JOIN
                        
                        event_foto USING (idevent)
                        
                        INNER JOIN
                        
                        user WHERE user.iduser = proses.iduser_initiator AND status = 1
                        
                        ORDER BY tanggalpembuatan DESC, waktupembuatan DESC"));
            $res['success'] = true;
            $res['total'] = count($aksi);
            $res['data'] = $aksi;
            $i++;

            return response($res,200);
        }

        public function newEventSearch(Request $request){
            $idUser = $request->iduser;
            $search = $request->search;
            $aksi = DB::select(
                DB::raw("SELECT idevent,namaevent,idfoto,urlfotoevent,tanggalpembuatan,waktupembuatan,iduser_initiator,fullname, status FROM
                        (
                            SELECT idevent,namaevent,tanggalpembuatan,waktupembuatan,iduser_initiator, status FROM event
                            WHERE
                            (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                OR 
                                (
                                idgrup IN
                                (
                                    SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                           AND idsubgrup='0'
                                )
                        
                                OR (idgrup='0' AND idsubgrup='0')
                        ) AS proses
                        
                        INNER JOIN
                        
                        event_foto USING (idevent)
                        
                        INNER JOIN
                        
                        user WHERE user.iduser = proses.iduser_initiator AND proses.namaevent LIKE '%$search%' AND status = 1
                        
                        ORDER BY tanggalpembuatan DESC, waktupembuatan DESC"));
            $res['success'] = true;
            $res['total'] = count($aksi);
            $res['data'] = $aksi;
            return response($res,200);
        }

        public function highlightEvent($idUser){
            $i = 0;
            $aksi = DB::select(
                DB::raw("SELECT idevent,namaevent,idfoto,urlfotoevent,tanggalpembuatan,waktupembuatan,iduser_initiator,fullname, status FROM
                        (
                            SELECT idevent,namaevent,tanggalpembuatan,waktupembuatan,iduser_initiator, status FROM event
                            WHERE
                            (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                OR 
                                (
                                idgrup IN
                                (
                                    SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                           AND idsubgrup='0'
                                )
                        
                                OR (idgrup='0' AND idsubgrup='0')
                        ) AS proses
                        
                        INNER JOIN
                        
                        event_foto USING (idevent)
                        
                        INNER JOIN
                        
                        user WHERE user.iduser = proses.iduser_initiator AND proses.iduser_initiator = 0 AND status = 1
                        
                        ORDER BY tanggalpembuatan DESC, waktupembuatan DESC"));
            $res['success'] = true;
            $res['total'] = count($aksi);
            $res['data'] = $aksi;
            $i++;

            return response($res,200);
        }

        public function highlightEventSearch(Request $request){
            $idUser = $request->iduser;
            $search = $request->search;
            $aksi = DB::select(
                DB::raw("SELECT idevent,namaevent,idfoto,urlfotoevent,tanggalpembuatan,waktupembuatan,iduser_initiator,fullname, status FROM
                        (
                            SELECT idevent,namaevent,tanggalpembuatan,waktupembuatan,iduser_initiator, status FROM event
                            WHERE
                            (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                OR 
                                (
                                idgrup IN
                                (
                                    SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                           AND idsubgrup='0'
                                )
                        
                                OR (idgrup='0' AND idsubgrup='0')
                        ) AS proses
                        
                        INNER JOIN
                        
                        event_foto USING (idevent)
                        
                        INNER JOIN
                        
                        user WHERE user.iduser = proses.iduser_initiator AND proses.namaevent LIKE '%$search%' AND proses.iduser_initiator = 0 AND status = 1
                        
                        ORDER BY tanggalpembuatan DESC, waktupembuatan DESC"));
            $res['success'] = true;
            $res['total'] = count($aksi);
            $res['data'] = $aksi;
            return response($res,200);
        }
    }