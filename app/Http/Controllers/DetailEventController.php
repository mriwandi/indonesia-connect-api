<?php
    namespace App\Http\Controllers;
    use App\DaftarJurusan;
    use App\Event;
    use App\EventAksi;
    use App\EventInvitation;
    use App\EventJoin;
    use App\User;
    use App\UserAktivitasEvent;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;

    class DetailEventController extends Controller
    {
        public function index($idEvent, $idUser){
            $event = Event::where('idevent', $idEvent)->with('eventFoto');
            if($event->count() != 0){
                $data['message'] = "Event Tersedia";
                $data['data'] = $event->first();
                $eventAksi = EventAksi::where('idevent', $idEvent);
                if($eventAksi->count() != 0){
                    $data['data']['idaksi'] = $eventAksi->first()->idaksi;
                }else{
                    $data['data']['idaksi'] = null;
                }
                $data['data']['owner'] = User::where('iduser', $event->first()->iduser_initiator)->first()->fullname;
                $data['data']['isAksi'] = count(EventAksi::where('idevent', $idEvent)->get()) != 0? true: false;
                $data['data']['isJoined'] = count(EventJoin::where('idevent', $idEvent)->where('iduser_join', $idUser)->get()) != 0? true: false;
                $data['data']['isOwner'] = count(Event::where('idevent', $idEvent)->where('iduser_initiator', $idUser)->get()) != 0? true: false;
                return response($data, 200);
            }else{
                $data['message'] = "Event tidak tersedia";
                $data['data'] = [];
                return response($data, 500);
            }
        }

        protected function getAktivitasEventId($id){
            return count(UserAktivitasEvent::where('iduser', $id)->get()) + 1;
        }

        public function join(Request $request){
            $tanggal = date("Y-m-d");
            $waktu = date("h:i:s");

            EventJoin::create([
                'idevent'       => $request->idevent,
                'iduser_join'   => $request->iduser,
                'tanggaljoin'   => $tanggal,
                'waktujoin'     => $waktu
            ]);

            UserAktivitasEvent::create([
                'iduser'            => $request->iduser,
                'idaktivitas'       => $this->getAktivitasEventId($request->iduser),
                'idevent'           => $request->idevent,
                'idjenisaktivitas'  => 8,
                'tanggalaktivitas'  => $tanggal,
                'waktuaktivitas'    => $waktu,
                'keterangan'        => 'Mengikuti acara'
            ]);

            $namaUser = User::where('iduser', $request->iduser)->first()->fullname;
            $namaAcara = Event::where('idevent', $request->idevent)->first()->namaevent;
            $userTarget = Event::where('idevent', $request->idevent)->first()->iduser_initiator;
            // \OneSignal::sendNotificationUsingTags(
            //     "$namaUser Mengikuti Acara $namaAcara",
            //     array(
            //         ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $userTarget]
            //     ),
            //     $url = null,
            //     $data = null,
            //     $buttons = null,
            //     $schedule = null
            // );

            $data['message'] = "Berhasil mengikuti acara";

            return response($data,200);
        }

        public function unjoin(Request $request){
            $event = EventJoin::where('idevent', $request->idevent)->where('iduser_join', $request->iduser);
            $event->delete();

            $res['success'] = true;
            $res['message'] = "Batal mengikuti acara";
            return response($res,200);
        }

        public function listPeserta($idEvent){
            $data['message'] = 'List Peserta Event';
            $data['data'] = EventJoin::where('idevent', $idEvent)->with('user.userDetail')->get();
            return response($data, 200);
        }

        public function aksiTerkait($idEvent){
            $aksi = EventAksi::where('idevent', $idEvent)->with('subaksi.subAksiFoto')->first();
            if($aksi){
                $data['isAksi'] = true;
                $data['message'] = 'List Aksi yang terkait';
                $data['data'] = $aksi;
            }else{
                $data['isAksi'] = false;
                $data['message'] = 'Tidak dicantumkan aksi';
            }
            return response($data, 200);
        }

        public function cekIdEvent($idaksi){
            $idevent = EventAksi::where('idaksi', $idaksi)->first();
            if($idevent){
                return response($idevent->idevent, 200);
            }else{
                $data = "Event tidak ditemukan";
                return response($data, 500);
            }
        }

        public function updateEvent(Request $request){
            $data = $request->all();
            $event = Event::where('idevent', $request->idevent);
            $event->update([
                'namaevent'             => $data['namaevent'],
                'tanggalawalevent'      => $data['tanggalawal'],
                'waktuawalevent'        => $data['waktuawal'],
                'tanggalakhirevent'     => $data['tanggalakhir'],
                'waktuakhirevent'       => $data['waktuakhir'],
                'lokasievent'           => $data['lokasi'],
            ]);

            if($data['deskripsi'] != ''){
                $event->update([
                   'deskripsi' => $data['deskripsi']
                ]);
            }
            return response($data, 200);
        }

        public function peringkat($idEvent){
            $idAksi = EventAksi::where('idevent', $idEvent)->first()->idaksi;
            $data = DB::select("SELECT angkatan,strata,kode_jurusan,SUM(nilaidonasi) AS jumlahdonasi FROM
	
                    (
                    SELECT iduser,nilaidonasi
                    FROM 
                        sub_aksi_aktivitas_donasi INNER JOIN user_aktivitas_donasi USING (iduser,idaktivitas)
                    WHERE 
                    idaksi='$idAksi'
                    ) AS temp
                
                    INNER JOIN 
                
                    user_jurusan 
                
                    USING (iduser)
                
                GROUP BY angkatan,strata,kode_jurusan
                ORDER BY jumlahdonasi DESC");

            $i = 0;
            foreach($data as $d){
//                dd($d);
                $data[$i]->jurusan = DaftarJurusan::where('kode_jurusan', $d->kode_jurusan)->first()->nama_jurusan;
                $i++;
            }

            return response($data, 200);
        }

        public function inviteList($idEvent, $idUser){
            $res = DB::select("SELECT iduser_following, fullname, urlfotoprofile FROM user_relasi u INNER JOIN user ON u.iduser_following = user.iduser INNER JOIN user_detail ON user.iduser = user_detail.iduser WHERE u.iduser = $idUser AND iduser_following NOT IN(SELECT iduser_invited FROM event_invitation WHERE idevent = $idEvent)");
            return response($res,200);
        }

        public function invite(Request $request, $idEvent){
            EventInvitation::create([
                'idevent'           => $idEvent,
                'iduser_invited'    => $request->get('iduser_invited'),
                'iduser_inviter'    => $request->get('iduser'),
                'status'            => 1
            ]);
            return response($request->all(), 200);
        }
    }