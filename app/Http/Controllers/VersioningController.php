<?php
    namespace App\Http\Controllers;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Http\Request;
    class VersioningController extends Controller
    {
        public function cekVersion(Request $request){
            $version = $request->get('version');

            $status = DB::select("SELECT idversi, status, keterangan FROM versi INNER JOIN versi_status ON versi.status = versi_status.versi WHERE versi.idversi = $version");

            return response($status, 200);
        }
    }