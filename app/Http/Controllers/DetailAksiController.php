<?php
namespace App\Http\Controllers;

use App\AksiAktivitasResponUsul;
use App\SubAksiAktivitasDonasiProses;
use App\SubAksiFoto;
use Illuminate\Support\Facades\DB;
use App\Aksi;
use App\AksiAktivitasUsul;
use App\AksiUsulVote;
use App\AksiFoto;
use App\User;
use App\UserAktivitas;
use App\UserAktivitasDonasi;
use App\UserAktivitasVolunteer;
use App\UserAktivitasUsul;
use App\SubAksiAktivitasDonasi;
use App\SubAksiAktivitasVolunteer;
use App\SubAksi;
use App\SubAksiDonasi;
use App\SubAksiVolunteer;
use Illuminate\Http\Request;

class DetailAksiController extends Controller {

    public function getData($idAksi){
//            DB::enableQueryLog();
        $aksi = Aksi::where('idaksi',$idAksi)->with('aksiFoto')->with('subAksi.jenisAksi')->with('subAksi.subAksiFoto')->with('subAksi.subAksiDonasi')->with('subAksi.subAksiVolunteer')->get();
        $i = 0;
        $res['success']             = true;
        $res['total']               = count($aksi);
        $res['data']                = $aksi;
        foreach($aksi as $act){
            $subAksiId = $act->subAksi[$i]->idsubaksi;
            $jumlahDonasi = DB::select(DB::raw("SELECT SUM(nilaidonasi) AS total FROM user_aktivitas_donasi INNER JOIN sub_aksi_aktivitas_donasi USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
            $jumlahVolunteer = DB::select(DB::raw("SELECT COUNT(iduser) AS total FROM user_aktivitas_volunteer INNER JOIN sub_aksi_aktivitas_volunteer USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
            $res['sub_aksi_detail'][$i]['idsubaksi'] = $subAksiId;
            if($act->subAksi[$i]->subAksiDonasi){
                $targetDonasi = $act->subAksi[$i]->subAksiDonasi->targetdonasi;
                $tanggalMulaiDonasi = $act->subAksi[$i]->subAksiDonasi->tanggalmulai;
                $tanggalAkhirDonasi = $act->subAksi[$i]->subAksiDonasi->tanggalselesai;
                $res['sub_aksi_detail'][$i]['tanggalmulaidonasi'] = $tanggalMulaiDonasi;
                $res['sub_aksi_detail'][$i]['tanggalakhirdonasi'] = $tanggalAkhirDonasi;
                $res['sub_aksi_detail'][$i]['totaldonasi'] = $jumlahDonasi[0]->total;
                $res['sub_aksi_detail'][$i]['targetdonasi'] = (int)$targetDonasi;
                if($targetDonasi != null){
                    $res['sub_aksi_detail'][$i]['persendonasi'] = round(($jumlahDonasi[0]->total/$targetDonasi)*100);
                } else {
                    $res['sub_aksi_detail'][$i]['persendonasi'] = ($jumlahDonasi[0]->total/1)*100;
                }
            }
            if($act->subAksi[$i]->subAksiVolunteer) {
                $targetVolunteer = $act->subAksi[$i]->subAksiVolunteer->targetvolunteer;
                $tanggalMulaiVolunteer = $act->subAksi[$i]->subAksiVolunteer->tanggalmulai;
                $tanggalAkhirVolunteer = $act->subAksi[$i]->subAksiVolunteer->tanggalselesai;
                $res['sub_aksi_detail'][$i]['tanggalmulaivolunteer'] = $tanggalMulaiVolunteer;
                $res['sub_aksi_detail'][$i]['tanggalakhirvolunteer'] = $tanggalAkhirVolunteer;
                $res['sub_aksi_detail'][$i]['totalvolunteer'] = $jumlahVolunteer[0]->total;
                $res['sub_aksi_detail'][$i]['targetvolunteer'] = (int)$targetVolunteer;
                if ($targetVolunteer != null) {
                    $res['sub_aksi_detail'][$i]['persenvolunteer'] = round(($jumlahVolunteer[0]->total / $targetVolunteer) * 100);
                } else {
                    $res['sub_aksi_detail'][$i]['persenvolunteer'] = ($jumlahVolunteer[0]->total / 1) * 100;
                }

            }

            $i++;
        }
        return response($res,200);
//            return dd(DB::getQueryLog());
    }

    private function isVolunteering($idAksi, $idSubAksi, $idUser){
        $data = UserAktivitas::where('iduser', $idUser)
            ->where('idaksi', $idAksi)
            ->where('idsubaksi', $idSubAksi)
            ->where('idjenisaktivitas', 3)->get();
        if(count($data) == 1)
            return true;
        else
            return false;
    }

    private function getSubAksiDonasiStatus($idAksi, $idSubAksi, $idUser){
        return SubAksiAktivitasDonasiProses::where([
            'idaksi'=> $idAksi,
            'idsubaksi' => $idSubAksi,
            'iduser' => $idUser
        ])->orderBy('idpayment', 'desc')->first();
    }

    public function subAksiDetail($idAksi, $idSubAksi, $idUser){
        $subAksi = SubAksi::where('sub_aksi.idaksi',$idAksi)->where('idsubaksi', $idSubAksi)->join('aksi_foto', 'aksi_foto.idaksi','=', 'sub_aksi.idaksi')->first();

        $data = $subAksi;
        $jumlahDonasi = DB::select(DB::raw("SELECT SUM(nilaidonasi) AS total FROM user_aktivitas_donasi INNER JOIN sub_aksi_aktivitas_donasi USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$idSubAksi."'"));
        $jumlahVolunteer = DB::select(DB::raw("SELECT COUNT(iduser) AS total FROM user_aktivitas_volunteer INNER JOIN sub_aksi_aktivitas_volunteer USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$idSubAksi."'"));
        $subAksiDonasi = DB::select(DB::raw("SELECT * FROM sub_aksi_donasi WHERE idaksi='".$idAksi."' AND idsubaksi='".$idSubAksi."'"));
        $subAksiVolunteer = DB::select(DB::raw("SELECT * FROM sub_aksi_volunteer WHERE idaksi='".$idAksi."' AND idsubaksi='".$idSubAksi."'"));

        $data['payment'] = $this->getSubAksiDonasiStatus($idAksi, $idSubAksi, $idUser);
        $data['sub_aksi_foto'] = SubAksiFoto::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->get();
        $data['owner'] = User::where('iduser', Aksi::where('idaksi', $idAksi)->first()->iduser_initiator)->first()->fullname;
        if(User::where('iduser', Aksi::where('idaksi', $idAksi)->first()->iduser_initiator)->first()->iduser == $idUser){
            $data['isOwner'] = true;
        }else{
            $data['isOwner'] = false;
        }
        if(count($subAksiDonasi) != 0){
            $data['sub_aksi_donasi']    = $subAksiDonasi[0];
            $data['persendonasi']       = round(($jumlahDonasi[0]->total/$subAksiDonasi[0]->targetdonasi)*100);
            $data['totaldonasi']        = $jumlahDonasi[0]->total;
        }else{
            $data['persendonasi']       = round(($jumlahDonasi[0]->total/1)*100);
            $data['totaldonasi']        = 0;
        }

        if(count($subAksiVolunteer) != 0){
            $data['sub_aksi_volunteer'] = $subAksiVolunteer[0];
            $data['persenvolunteer']    = round(($jumlahVolunteer[0]->total/$subAksiVolunteer[0]->targetvolunteer)*100);
            $data['totalvolunteer']     = $jumlahVolunteer[0]->total;
            $data['mengikuti']          = $this->isVolunteering($idAksi, $idSubAksi, $idUser);
        }else{
            $data['persenvolunteer']    = round(($jumlahVolunteer[0]->total/1)*100);
            $data['totalvolunteer']     = 0;
        }

        $res['success']             = true;
        $res['idAksi']              = $idAksi;
        $res['idSubAksi']           = $idSubAksi;
        $res['data']                = $data;

        return response($res,200);
    }

    public function donate(Request $request){
        $data['iduser']             = $request->get('iduser');
        $data['idaksi']             = $request->get('idaksi');
        $data['idsubaksi']          = $request->get('idsubaksi');
        $data['idjenisaktivitas']   = $request->get('idjenisaktivitas');
        $data['tanggalaktivitas']   = date("Y-m-d");
        $data['waktuaktivitas']     = date("h:i:s");
        $data['keterangan']         = $request->get('keterangan');
        $data['nilaidonasi']        = $request->get('nilaidonasi');
        $data['keteranganDonasi']   = $request->get('keteranganDonasi');
        $data['idaktivitas']        = $this->getAktivitasId($data['iduser']);
        $data['iddonasi']           = $this->getDonateId($data['idaksi'], $data['idsubaksi']);

        $this->createDonate($data);
        $res['success'] = true;
        $res['message'] = "Berhasil menambahkan Donasi";
        $res['data'] = $data;
        return response($res,200);
    }

    protected function createDonate(array $data){
        UserAktivitas::create([
            'iduser'            => $data['iduser'],
            'idaktivitas'       => $data['idaktivitas'],
            'idaksi'            => $data['idaksi'],
            'idsubaksi'         => $data['idsubaksi'],
            'idjenisaktivitas'  => $data['idjenisaktivitas'],
            'tanggalaktivitas'  => $data['tanggalaktivitas'],
            'waktuaktivitas'    => $data['waktuaktivitas'],
            'keterangan'        => $data['keterangan']
        ]);

        UserAktivitasDonasi::create([
            'iduser'        => $data['iduser'],
            'idaktivitas'   => $data['idaktivitas'],
            'nilaidonasi'   => $data['nilaidonasi'],
            'keterangan'    => $data['keteranganDonasi']
        ]);

        SubAksiAktivitasDonasi::create([
            'idaksi'        => $data['idaksi'],
            'idsubaksi'     => $data['idsubaksi'],
            'iddonasi'      => $data['iddonasi'],
            'iduser'        => $data['iduser'],
            'idaktivitas'   => $data['idaktivitas']
        ]);
    }

    protected function getDonateId($idaksi, $idsubaksi){
        return count(SubAksiAktivitasDonasi::where('idaksi', $idaksi)->where('idsubaksi', $idsubaksi)->get()) + 1;
    }

    protected function getAktivitasId($id){
        return DB::select('SELECT max(idaktivitas) AS id FROM user_aktivitas WHERE iduser = '.$id)[0]->id + 1;
    }

    public function volunteering(Request $request){
        $data['iduser']                 = $request->get('iduser');
        $data['idaksi']                 = $request->get('idaksi');
        $data['idsubaksi']              = $request->get('idsubaksi');
        $data['idjenisaktivitas']       = $request->get('idjenisaktivitas');
        $data['tanggalaktivitas']       = date("Y-m-d");
        $data['waktuaktivitas']         = date("h:i:s");
        $data['keterangan']             = $request->get('keterangan');
        $data['tanggalawalkesediaan']   = $request->get('tanggalawalkesediaan');
        $data['tanggalakhirkesediaan']  = $request->get('tanggalakhirkesediaan');
        $data['waktuawalkesediaan']     = $request->get('waktuawalkesediaan');
        $data['waktuakhirkesediaan']    = $request->get('waktuakhirkesediaan');
        $data['handphone']              = $request->get('handphone');
        $data['idaktivitas']            = $this->getAktivitasId($data['iduser']);
        $data['idvolunteer']            = $this->getVolunteerId($data['idaksi'], $data['idsubaksi']);

        $this->createVolunteering($data);

        $namaUser = User::where('iduser', $data['iduser'])->first()->fullname;
        $namaAksi = Aksi::where('idaksi', $data['idaksi'])->first()->namaaksi;
        $userTarget = Aksi::where('idaksi', $data['idaksi'])->first()->iduser_initiator;
        // \OneSignal::sendNotificationUsingTags(
        //     "$namaUser Menjadi Relawan di Aksi $namaAksi",
        //     array(
        //         ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $userTarget]
        //     ),
        //     $url = null,
        //     $data = null,
        //     $buttons = null,
        //     $schedule = null
        // );

        $res['success'] = true;
        $res['message'] = "Berhasil menjadi volunteer";
        $res['data'] = $data;
        return response($res,200);
    }

    protected function getVolunteerId($idaksi, $idsubaksi){
        return count(SubAksiAktivitasVolunteer::where('idaksi', $idaksi)->where('idsubaksi', $idsubaksi)->get()) + 1;
    }

    protected function createVolunteering(array $data){

        UserAktivitas::create([
            'iduser'            => $data['iduser'],
            'idaktivitas'       => $data['idaktivitas'],
            'idaksi'            => $data['idaksi'],
            'idsubaksi'         => $data['idsubaksi'],
            'idjenisaktivitas'  => $data['idjenisaktivitas'],
            'tanggalaktivitas'  => $data['tanggalaktivitas'],
            'waktuaktivitas'    => $data['waktuaktivitas'],
            'keterangan'        => $data['keterangan']
        ]);

        UserAktivitasVolunteer::create([
            'iduser'                => $data['iduser'],
            'idaktivitas'           => $data['idaktivitas'],
            'tanggalawalkesediaan'  => $data['tanggalawalkesediaan'],
            'tanggalakhirkesediaan' => $data['tanggalakhirkesediaan'],
            'waktuawalkesediaan'    => $data['waktuawalkesediaan'],
            'waktuakhirkesediaan'   => $data['waktuakhirkesediaan'],
            'handphone'             => $data['handphone']
        ]);

        SubAksiAktivitasVolunteer::create([
            'idaksi'        => $data['idaksi'],
            'idsubaksi'     => $data['idsubaksi'],
            'idvolunteer'   => $data['idvolunteer'],
            'iduser'        => $data['iduser'],
            'idaktivitas'   => $data['idaktivitas']
        ]);
    }

    public function getReport($idAksi){
        $aksi           = DB::select("SELECT * FROM aksi_report WHERE idaksi = $idAksi ORDER BY idreport DESC");
        $aksi = json_decode(json_encode($aksi), true);
        $i = 0;
        foreach($aksi as $a){
            $idReport   = $a['idreport'];
            $deskripsi  = DB::select("SELECT * FROM aksi_report_deskriptif WHERE idreport = $idReport AND idaksi = $idAksi");
            $transaksi  = DB::select("SELECT * FROM aksi_report_transaksi WHERE idreport = $idReport AND idaksi = $idAksi");
            $foto       = DB::select("SELECT * FROM aksi_report_foto WHERE idreport = $idReport AND idaksi = $idAksi");
            $aksi[$i]['deskripsi']  = json_decode(json_encode($deskripsi), true);
            $aksi[$i]['transaksi']  = json_decode(json_encode($transaksi), true);
            $aksi[$i]['foto']       = json_decode(json_encode($foto), true);
            $total = 0;
            foreach($aksi[$i]['transaksi'] as $t){
                $aksi[$i]['total']  = $total += $t['nilaitransaksi'];
            }
            $i++;
        }
        $res['success'] = true;
        $res['total']   = count($aksi);
        $res['data']    = $aksi;

        return response($res,200);
    }

    protected function getFotoId($idaksi){
        return count(SubAksiAktivitasVolunteer::where('idaksi', $idaksi)->get()) + 1;
    }

    public function insertAksi(Request $request){
        $data                       = $request->all();
        $data['idaktivitas']        = $this->getAktivitasId($data['iduser_initiator']);
        $data['idaksi']             = $this->getAksiId();
        $data['idfoto']             = $this->getFotoId($data['idaksi']);
        if($data['tipeaksi'] == '1'){
            $data['idsubaksi']          = $this->getSubAksiId($data['idaksi']);
        }else if($data['tipeaksi'] == '2'){
            $data['idsubaksi']          = 0;
        }

        if($request->hasFile('urlfotoaksi')){
            $image = $request->file('urlfotoaksi');
            $filename = "aksi". $data['idaksi'] . '.'.$image->getClientOriginalExtension();
            $done = $image->move(public_path("/upload/aksi/"), $filename);
            $url = url('/') . '/upload/aksi/' . $filename;
            $data['urlfotoaksi'] = $url;
        }else{
            $data['urlfotoaksi'] = null;
        }

        $data['idjenisaktivitas']   = 1;
        $data['tanggalpembuatan']   = date("Y-m-d");
        $data['waktupembuatan']     = date("h:i:s");
        $data['keterangan']         = "Membuat suatu Aksi";

        $this->createAksi($data);
        $res['success'] = true;
        $res['total']   = count($data);
        $res['data']    = $data;

        return response($res,200);
    }

    protected function createAksi(array $data){
        Aksi::create([
            'idaksi'            => $data['idaksi'],
            'iduser_initiator'  => $data['iduser_initiator'],
            'idgrup'            => $data['idgrup'],
            'idsubgrup'         => $data['idsubgrup'],
            'namaaksi'          => $data['namaaksi'],
            'deskripsi'         => $data['deskripsi'],
            'tanggalpembuatan'  => $data['tanggalpembuatan'],
            'waktupembuatan'    => $data['waktupembuatan'],
            'status'            => 1,
            'tipeaksi'          => $data['tipeaksi']
        ]);

        AksiFoto::create([
            'idaksi'            => $data['idaksi'],
            'idfoto'            => $data['idfoto'],
            'urlfotoaksi'       => $data['urlfotoaksi']
        ]);

        SubAksi::create([
            'idaksi'            => $data['idaksi'],
            'idsubaksi'         => $data['idsubaksi'],
            'idjenisaksi'       => $data['idjenisaksi'],
            'namasubaksi'       => $data['namasubaksi'],
            'deskripsisubaksi'  => $data['deskripsisubaksi'],
            'tanggalpembuatan'  => $data['tanggalpembuatan'],
            'waktupembuatan'    => $data['waktupembuatan']
        ]);

        if($data['donasistatus'] == 'true'){
            SubAksiDonasi::create([
                'idaksi'            => $data['idaksi'],
                'idsubaksi'         => $data['idsubaksi'],
                'tanggalmulai'      => $data['donasitanggalmulai'],
                'tanggalselesai'    => $data['donasitanggalselesai'],
                'targetdonasi'      => $data['donasitargetdonasi']
            ]);
        }

        if($data['relawanstatus'] == 'true'){
            SubAksiVolunteer::create([
                'idaksi'                => $data['idaksi'],
                'idsubaksi'             => $data['idsubaksi'],
                'tanggalmulai'          => $data['relawantanggalmulai'],
                'tanggalselesai'        => $data['relawantanggalselesai'],
                'tanggalmulaiaksi'      => $data['relawantanggalmulaiaksi'],
                'tanggalselesaiaksi'    => $data['relawantanggalselesaiaksi'],
                'waktumulaiaksi'        => $data['relawanwaktumulaiaksi'],
                'waktuselesaiaksi'      => $data['relawanwaktuselesaiaksi'],
                'targetvolunteer'       => $data['relawantargetvolunteer']
            ]);
        }

        UserAktivitas::create([
            'iduser'                => $data['iduser_initiator'],
            'idaktivitas'           => $data['idaktivitas'],
            'idaksi'                => $data['idaksi'],
            'idsubaksi'             => $data['idsubaksi'],
            'idjenisaktivitas'      => $data['idjenisaktivitas'],
            'tanggalaktivitas'      => $data['tanggalpembuatan'],
            'waktuaktivitas'        => $data['waktupembuatan'],
            'keterangan'            => $data['keterangan']
        ]);
    }

    protected function getAksiId(){
        return count(Aksi::all()) + 1;
    }

    protected function getSubAksiId($idaksi){
        return count(SubAksi::where('idaksi', $idaksi)->get()) + 1;
    }

    protected function getUsulId($idaksi){
        return count(AksiAktivitasUsul::where('idaksi', $idaksi)->get()) + 1;
    }

    public function insertUsul(Request $request){
        $data                       = $request->all();
        $data['idaktivitas']        = $this->getAktivitasId($data['iduser']);
        $data['idusul']             = $this->getUsulId($data['idaksi']);
        if($request->tipeaksi == 1){
            $data['idsubaksi']      = 1;
        }else{
            $data['idsubaksi']      = 0;
        }
        $data['idjenisaktivitas']   = 4;
        $data['tanggalpembuatan']   = date("Y-m-d");
        $data['waktupembuatan']     = date("h:i:s");
        $data['keterangan']         = "Memberikan usul";

//        dd($data);
        $this->createUsul($data);

        $res['success'] = true;
        $res['data']    = $data;

        return response($res,200);
    }

    protected function createUsul($data){
        UserAktivitas::create([
            'iduser'            => $data['iduser'],
            'idaktivitas'       => $data['idaktivitas'],
            'idaksi'            => $data['idaksi'],
            'idsubaksi'         => $data['idsubaksi'],
            'idjenisaktivitas'  => $data['idjenisaktivitas'],
            'tanggalaktivitas'  => $data['tanggalpembuatan'],
            'waktuaktivitas'    => $data['waktupembuatan'],
            'keterangan'        => $data['keterangan']
        ]);

        UserAktivitasUsul::create([
            'iduser'        => $data['iduser'],
            'idaktivitas'   => $data['idaktivitas'],
            'isi'           => $data['isi']
        ]);

        AksiAktivitasUsul::create([
            'idaksi'        => $data['idaksi'],
            'idusul'        => $data['idusul'],
            'iduser'        => $data['iduser'],
            'idaktivitas'   => $data['idaktivitas']
        ]);

        $namaUser = User::where('iduser', $data['iduser'])->first()->fullname;
        $namaAksi = Aksi::where('idaksi', $data['idaksi'])->first()->namaaksi;
        $userTarget = Aksi::where('idaksi', $data['idaksi'])->first()->iduser_initiator;
        // \OneSignal::sendNotificationUsingTags(
        //     "$namaUser Memberi Saran dalam Aksi $namaAksi",
        //     array(
        //         ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $userTarget]
        //     ),
        //     $url = null,
        //     $data = null,
        //     $buttons = null,
        //     $schedule = null
        // );
    }

    protected function getResponId($idUsul){
        return count(AksiAktivitasResponUsul::where('idusul', $idUsul)->get());
    }

    public function replyUsul(Request $request){
        $data                   = $request->all();
        $data['idrespon']       = $this->getResponId($data['idusul']);
        $data['idaktivitas']    = $this->getAktivitasId($data['iduser']);
        if($request->tipeaksi == 1){
            $data['idsubaksi']      = 1;
        }else{
            $data['idsubaksi']      = 0;
        }
        $data['idjenisaktivitas']   = 5;
        $data['tanggalpembuatan']   = date("Y-m-d");
        $data['waktupembuatan']     = date("h:i:s");
        $data['keterangan']         = "Memberikan respon usul";

        $this->createReplyUsul($data);
        $res['success'] = true;
        $res['data']    = $data;
        return response($res, 200);
    }

    protected function createReplyUsul($data){
        UserAktivitas::create([
            'iduser'            => $data['iduser'],
            'idaktivitas'       => $data['idaktivitas'],
            'idaksi'            => $data['idaksi'],
            'idsubaksi'         => $data['idsubaksi'],
            'idjenisaktivitas'  => $data['idjenisaktivitas'],
            'tanggalaktivitas'  => $data['tanggalpembuatan'],
            'waktuaktivitas'    => $data['waktupembuatan'],
            'keterangan'        => $data['keterangan']
        ]);

        UserAktivitasUsul::create([
            'iduser'        => $data['iduser'],
            'idaktivitas'   => $data['idaktivitas'],
            'isi'           => $data['isi']
        ]);

        AksiAktivitasResponUsul::create([
            'idaksi'        => $data['idaksi'],
            'idusul'        => $data['idusul'],
            'idrespon'      => $data['idrespon'],
            'iduser'        => $data['iduser'],
            'idaktivitas'   => $data['idaktivitas']
        ]);
    }

    protected function getUpvoted($idaksi, $idusul, $iduser){
        $upvote = AksiUsulVote::where('idaksi', $idaksi)->where('idusul', $idusul)->where('iduservote', $iduser)->get();
        if(count($upvote) > 0){
            return true;
        }else{
            return false;
        }
    }

    protected function getUpvoteCount($idaksi, $idusul){
        $upvote = AksiUsulVote::where('idaksi', $idaksi)->where('idusul', $idusul)->get();
        return count($upvote);
    }

    public function usul($idAksi, $idUser){
//        $query  = DB::select(DB::raw("SELECT * FROM aksi_aktivitas_usul AS aksi INNER JOIN user_aktivitas_usul USING(iduser, idaktivitas) INNER JOIN user_aktivitas USING(iduser, idaktivitas) WHERE aksi.idaksi = $idAksi ORDER BY idusul DESC"));
        $query  = AksiAktivitasUsul::with('userAktivitasUsul.userAktivitas')->where('idaksi',$idAksi)->orderBy('idusul','desc')->get();
        $id = 0;
        foreach($query as $q){
            $query[$id]->reply      = AksiAktivitasResponUsul::where('idusul', $q->idusul)->where('idaksi', $idAksi)->with('userAktivitasUsul.userAktivitas')->with('user.userDetail')->get();
            $query[$id]->user       = User::where('iduser', $q->iduser)->with('userDetail')->first();
            $query[$id]->upvoted    = $this->getUpvoted($q->idaksi, $q->idusul, $idUser);
            $query[$id]->upvote     = $this->getUpvoteCount($q->idaksi, $q->idusul);
            $id++;
        }
        $res['success'] = true;
        $res['data']    = $query;

        return response($res, 200);
    }

    protected function getIdVote($idAksi, $idUsul) {
        return count(AksiUsulVote::where('idaksi', $idAksi)->where('idusul', $idUsul)->get());
    }

    public function upvote(Request $request){
        $idAktivitas = $this->getAktivitasId($request->iduser);
        if($request->tipeaksi == 1){
            $idSubAksi      = 1;
        }else{
            $idSubAksi      = 0;
        }
        AksiUsulVote::create([
            'idaksi'        => $request->idaksi,
            'idusul'        => $request->idusul,
            'idvote'        => $this->getIdVote($request->idaksi, $request->idusul),
            'iduservote'    => $request->iduser,
            'tanggalvote'   => date("Y-m-d"),
            'waktuvote'     => date("h:i:s"),
            'isvoteaktif'  => 1
        ]);

        UserAktivitas::create([
            'iduser'            => $request->iduser,
            'idaktivitas'       => $idAktivitas,
            'idaksi'            => $request->idaksi,
            'idsubaksi'         => $idSubAksi,
            'idjenisaktivitas'  => 6,
            'tanggalaktivitas'  => date("Y-m-d"),
            'waktuaktivitas'    => date("h:i:s"),
            'keterangan'        => "Upvote Usulan"
        ]);

        $res['success'] = true;
        $res['data']    = $request->all();

        return response($res, 200);
    }

    public function unvote(Request $request){
        $idaksi = $request->idaksi;
        $idusul = $request->idusul;
        $iduser = $request->get('iduser');

        $relasi = AksiUsulVote::where('idaksi', $idaksi)->where('idusul', $idusul)->where('iduservote', $iduser);
        $relasi->delete();

        $res['success'] = true;
        $res['message'] = "Berhasil unvote";
        return response($res,200);
    }

    public function cekBlock($idAksi){
        $query = Aksi::select('status')->where('idaksi', $idAksi)->first()->status;
        if($query != 1){
            return response("true", 200);
        }else{
            return response("false", 500);
        }
    }

    public function ubahAksi(Request $request){
        $data                       = $request->all();
        if($data['tipeaksi'] == '1'){
            $data['idsubaksi']          = $this->getSubAksiId($data['idaksi']);
        }else if($data['tipeaksi'] == '2'){
            $data['idsubaksi']          = 0;
        }

        $this->fungsiUbah($data);
        $res['success'] = true;
        $res['total']   = count($data);
        $res['data']    = $data;

        return response($res,200);
    }

    protected function fungsiUbah(array $data){
        $aksi = Aksi::where('idaksi', $data['idaksi']);
        $subaksi = SubAksi::where('idaksi', $data['idaksi'])->where('idsubaksi', $data['idsubaksi']);


        $aksi->update([
            'namaaksi'          => $data['namaaksi'],
            'deskripsi'         => $data['deskripsi']
        ]);


        $subaksi->update([
            'idjenisaksi'       => $data['idjenisaksi'],
            'namasubaksi'       => $data['namasubaksi'],
            'deskripsisubaksi'  => $data['deskripsisubaksi']
        ]);

        if($data['donasi']['status']){
            $subAksiDonasi = SubAksiDonasi::where('idaksi', $data['idaksi'])->where('idsubaksi', $data['idsubaksi']);
            if($subAksiDonasi->count() == 0){
                SubAksiDonasi::create([
                    'idaksi'            => $data['idaksi'],
                    'idsubaksi'         => $data['idsubaksi'],
                    'tanggalmulai'      => $data['donasi']['tanggalmulai'],
                    'tanggalselesai'    => $data['donasi']['tanggalselesai'],
                    'targetdonasi'      => $data['donasi']['targetdonasi']
                ]);
            }else{
                $subAksiDonasi->update([
                    'tanggalmulai'      => $data['donasi']['tanggalmulai'],
                    'tanggalselesai'    => $data['donasi']['tanggalselesai'],
                    'targetdonasi'      => $data['donasi']['targetdonasi']
                ]);
            }
        }

        if($data['volunteer']['status']){
            $subAksiVolunteer = SubAksiVolunteer::where('idaksi', $data['idaksi'])->where('idsubaksi', $data['idsubaksi']);
            if($subAksiVolunteer->count() == 0){
                SubAksiVolunteer::create([
                    'idaksi'                => $data['idaksi'],
                    'idsubaksi'             => $data['idsubaksi'],
                    'tanggalmulai'          => $data['volunteer']['tanggalmulai'],
                    'tanggalselesai'        => $data['volunteer']['tanggalselesai'],
                    'tanggalmulaiaksi'      => $data['volunteer']['tanggalmulaiaksi'],
                    'tanggalselesaiaksi'    => $data['volunteer']['tanggalselesaiaksi'],
                    'waktumulaiaksi'        => $data['volunteer']['waktumulaiaksi'],
                    'waktuselesaiaksi'      => $data['volunteer']['waktuselesaiaksi'],
                    'targetvolunteer'       => $data['volunteer']['targetvolunteer']
                ]);
            }else{
                $subAksiVolunteer->update([
                    'tanggalmulai'          => $data['volunteer']['tanggalmulai'],
                    'tanggalselesai'        => $data['volunteer']['tanggalselesai'],
                    'tanggalmulaiaksi'      => $data['volunteer']['tanggalmulaiaksi'],
                    'tanggalselesaiaksi'    => $data['volunteer']['tanggalselesaiaksi'],
                    'waktumulaiaksi'        => $data['volunteer']['waktumulaiaksi'],
                    'waktuselesaiaksi'      => $data['volunteer']['waktuselesaiaksi'],
                    'targetvolunteer'       => $data['volunteer']['targetvolunteer']
                ]);
            }

        }
    }
}