<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubAksiAktivitasDonasi;
use App\SubAksiAktivitasDonasiProses;
use App\UserAktivitas;
use App\UserAktivitasDonasi;
use App\User;
use App\Aksi;
use Veritrans_Config;
use Veritrans_Snap;
use Veritrans_Notification;

class PaymentController extends Controller
{
    function __construct()
    {
        Veritrans_Config::$isProduction = config('midtrans.is_production');
        Veritrans_Config::$serverKey = config('midtrans.server_key');
        Veritrans_Config::$isSanitized = config('midtrans.is_sanitized');
        Veritrans_Config::$is3ds = config('midtrans.is_3ds');
    }

    private function generatePaymentID($idaksi, $idsubaksi, $iduser)
    {
        return count(SubAksiAktivitasDonasiProses::where([
            'idaksi' => $idaksi,
            'idsubaksi' => $idsubaksi,
            'iduser' => $iduser
        ])->get()) + 1;
    }

    private function validate_signature_key($response)
    {
        $signature_string = $response->order_id . $response->status_code . $response->gross_amount . config('midtrans.server_key');
        return openssl_digest($signature_string, 'sha512') === $response->signature_key;
    }

    public function donate(Request $request)
    {
        $this->validate($request, [
            'idaksi' => 'required|numeric',
            'idsubaksi' => 'required|numeric',
            'iduser' => 'required|numeric',
            'besaran' => 'required|numeric'
        ]);

        $find_aksi = Aksi::where('idaksi', $request->idaksi);
        $item_name = 'Aksi Donasi';
        if($find_aksi->count() > 0) {
            $aksi = $find_aksi->first();
            $item_name = 'Donasi untuk aksi';
        }

        $users = User::where('iduser', $request->iduser);
        if($users->count() > 0) {
            $user = $users->first();
            $fullname = $user->fullname;
            $first_name = $fullname;
            $last_name = '';
            $email = $user->email;
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'User not found in our system.'
            ], 404);
        }

        $last_transaction = SubAksiAktivitasDonasiProses::where('idaksi',$request->idaksi)
            ->where('idsubaksi', $request->idsubaksi)
            ->where('iduser', $request->iduser)
            ->whereNull('payment_status')
            ->orWhere('payment_status','Pending')
            ->orWhere('payment_status','Challenge');

        if($last_transaction->count() > 0) {
            $last_payment = $last_transaction->first();
            return response()->json([
                'status' => 200,
                'message' => 'Success retrieving last token',
                'token_id' => $last_payment->token_id
            ]);
        } else {
            $order_id = rand();
            $transaction_details = [
                'order_id' => $order_id,
                'gross_amount' => $request->besaran
            ];
            $item_details = array(
                array(
                    'id' => $request->idaksi,
                    'name' => $item_name,
                    'quantity' => 1,
                    'price' => $request->besaran
                ),
            );
            $customer_details = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
            );
            $enabled_payments = [
                "credit_card",
                "gopay",
                "mandiri_clickpay",
                "cimb_clicks",
                "bca_klikbca",
                "bca_klikpay",
                "bri_epay",
                "telkomsel_cash",
                "echannel",
                "bbm_money",
                "xl_tunai",
                "indosat_dompetku",
                "mandiri_ecash",
                "permata_va",
                "bca_va",
                "bni_va",
                "danamon_online",
                "other_va",
                "kioson",
                "Indomaret"
            ];

            $custom_expiry = [
                'start_time' => date("Y-m-d H:i:s O", time()),
                'unit' => 'day',
                'duration' => 1
            ];

            $transaction = array(
                'transaction_details' => $transaction_details,
                'customer_details' => $customer_details,
                'item_details' => $item_details,
                'expiry' => $custom_expiry
            );

            $token = Veritrans_Snap::getSnapToken($transaction);
            SubAksiAktivitasDonasiProses::create([
                'idaksi'            => $request->idaksi,
                'idsubaksi'         => $request->idsubaksi,
                'iduser'            => $request->iduser,
                'idpayment'         => $this->generatePaymentID($request->idaksi, $request->idsubaksi, $request->iduser),
                'nilaidonasi'       => $request->besaran,
                'status'            => 0,
                'token_id'          => $token,
                'order_id'          => $order_id
            ]);

            return response()->json([
                'status' => 200,
                'message' => 'Success requesting token for transaction',
                'token_id' => $token
            ]);
        }        
    }

    /**
     * Fungsi untuk memproses response/notifikasi dari midtrans setelah selesai transaksi
     * 
     */
    public function notification(Request $request)
    {
        $notif = new Veritrans_Notification();
        if ($this->validate_signature_key($notif)) {
            $transaction = $notif->transaction_status;
            $type = $notif->payment_type;
            $order_id = $notif->order_id;
            $fraud = $notif->fraud_status;
            $payment_status = null;
            $status = false;
            $message = '';
            $bank = null;
            $va_number = null;
            if($type == 'credit_card') {
                $bank = $notif->bank;
            } else if($type == 'bank_transfer') {
                if(isset($notif->va_numbers)){
                    $bank = $notif->va_numbers[0]->bank;
                    $va_number = $notif->va_numbers[0]->va_number;
                } else {
                    if(isset($notif->permata_va_number)) {
                        $bank = 'permata';
                        $va_number = $notif->permata_va_number;
                    }
                }
            } else if($type == 'echannel' or $type == 'mandiri_clickpay' or $type == 'mandiri_ecash') {
                $bank = 'mandiri';
                $va_number = $notif->biller_code.$notif->bill_key;
            } else if($type == 'bca_klikbca' or $type == 'bca_klikpay') {
                $bank = 'bca';
            } else if($type == 'cimb_clicks') {
                $bank = 'cimb';
            } else if($type == 'danamon_online') {
                $bank = 'danamon';
            } else if($type == 'bri_epay') {
                $bank = 'bri';
            } else if($type == 'cstore') {
                $bank = 'indomaret';
            } else if($type == 'gopay') {
                $bank = 'gopay';
            }
            if ($transaction == 'capture') {
                // Untuk transaksi kartu kredit, harus dicek apakah melewati challenge dari FDS atau tidak
                if ($type == 'credit_card') {
                    if ($fraud == 'challenge') {
                        // TODO - Atur status transaksi di database merchant menjadi 'Challenge by FDS'
                        // TODO - Merchant harus memutuskan untuk menerima/menolak transaksi di dashboard Midtrans (MAP)
                        $status = false;
                        $payment_status = 'Challenge';
                        $message = "Transaction order_id: " . $order_id . " is challenged by FDS";
                    } else {
                        // TODO - Atur status transaksi di database merhant menjadi 'Success'
                        $status = true;
                        $payment_status = 'Success';
                        $message = "Transaction order_id: " . $order_id . " successfully captured using " . $type;
                    }
                }
            } else if ($transaction == 'settlement') {
                // TODO - Atur status transaksi di database merhant menjadi 'Settlement'
                $status = true;
                $payment_status = 'Settlement';
                $message = "Transaction order_id: " . $order_id . " successfully transfered using " . $type;
            } else if ($transaction == 'pending') {
                // TODO - Atur status transaksi di database merhant menjadi 'Pending'
                $status = false;
                $payment_status = 'Pending';
                $message = "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
            } else if ($transaction == 'deny') {
                // TODO - Atur status transaksi di database merhant menjadi 'Denied'
                $status = false;
                $payment_status = 'Denied';
                $message = "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
            } else if ($transaction == 'expire') {
                // TODO - Atur status transaksi di database merhant menjadi 'expire'
                $status = false;
                $payment_status = 'Expire';
                $message = "Payment using " . $type . " for transaction order_id: " . $order_id . " is expired.";
            } else if ($transaction == 'cancel') {
                // TODO - Atur status transaksi di database merhant menjadi 'Denied'
                $status = false;
                $payment_status = 'Denied';
                $message = "Payment using " . $type . " for transaction order_id: " . $order_id . " is canceled.";
            }

            $transaksi = SubAksiAktivitasDonasiProses::where('order_id', $order_id);
            if($transaksi->count() > 0) {
                $transaksi->update([
                    'idtransaksi' => $notif->transaction_id,
                    'banktujuan' => $bank,
                    'status' => (($payment_status == 'Settlement') or ($payment_status == 'Success')) ? '1' : '0',
                    'payment_type' => $type,
                    'payment_status' => $payment_status,
                ]);
                if($payment_status == 'Settlement' or $payment_status == 'Success') {

                    $result = $transaksi->first();
                    $find_aksi = Aksi::where('idaksi', $result->idaksi);
                    $keterangan = 'Aksi Donasi';
                    if($find_aksi->count() > 0) {
                        $aksi = $find_aksi->first();
                        $keterangan = 'Donasi untuk aksi '.$aksi->namaaksi;
                    }

                    $namaUser = User::where('iduser', $result->iduser)->first()->fullname;
                    $namaAksi = Aksi::where('idaksi', $result->idaksi)->first()->namaaksi;
                    $userTarget = Aksi::where('idaksi', $result->idaksi)->first()->iduser_initiator;
                    // \OneSignal::sendNotificationUsingTags(
                    //     "$namaUser Berdonasi di Aksi $namaAksi",
                    //     array(
                    //         ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $userTarget]
                    //     ),
                    //     $url = null,
                    //     $data = null,
                    //     $buttons = null,
                    //     $schedule = null
                    // );

                    // \OneSignal::sendNotificationUsingTags(
                    //     "Pembayaran Untuk Donasi Aksi $namaAksi Berhasil Diterima",
                    //     array(
                    //         ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $result->iduser]
                    //     ),
                    //     $url = null,
                    //     $data = null,
                    //     $buttons = null,
                    //     $schedule = null
                    // );
    
                    $data['iduser']             = $result->iduser;
                    $data['idaksi']             = $result->idaksi;
                    $data['idsubaksi']          = $result->idsubaksi;
                    $data['idjenisaktivitas']   = 2;
                    $data['tanggalaktivitas']   = date("Y-m-d");
                    $data['waktuaktivitas']     = date("h:i:s");
                    $data['keterangan']         = $keterangan;
                    $data['nilaidonasi']        = $result->nilaidonasi;
                    $data['keteranganDonasi']   = $keterangan;
                    $data['idaktivitas']        = $this->getAktivitasId($data['iduser']);
                    $data['iddonasi']           = $this->getDonateId($data['idaksi'], $data['idsubaksi']);
    
                    $this->createDonate($data);
                }
            } else {
                $message .= ' Transaction not found on merchant database.';
            }            

            return response()->json(['status' => $status, 'message' => $message]);
        }
    }

    protected function getDonateId($idaksi, $idsubaksi){
        return count(SubAksiAktivitasDonasi::where('idaksi', $idaksi)->where('idsubaksi', $idsubaksi)->get()) + 1;
    }

    protected function getAktivitasId($id){
        return count(UserAktivitas::where('iduser', $id)->get()) + 1;
    }

    protected function createDonate(array $data){
        UserAktivitas::create([
            'iduser'            => $data['iduser'],
            'idaktivitas'       => $data['idaktivitas'],
            'idaksi'            => $data['idaksi'],
            'idsubaksi'         => $data['idsubaksi'],
            'idjenisaktivitas'  => $data['idjenisaktivitas'],
            'tanggalaktivitas'  => $data['tanggalaktivitas'],
            'waktuaktivitas'    => $data['waktuaktivitas'],
            'keterangan'        => $data['keterangan']
        ]);

        UserAktivitasDonasi::create([
            'iduser'        => $data['iduser'],
            'idaktivitas'   => $data['idaktivitas'],
            'nilaidonasi'   => $data['nilaidonasi'],
            'keterangan'    => $data['keteranganDonasi']
        ]);

        SubAksiAktivitasDonasi::create([
            'idaksi'        => $data['idaksi'],
            'idsubaksi'     => $data['idsubaksi'],
            'iddonasi'      => $data['iddonasi'],
            'iduser'        => $data['iduser'],
            'idaktivitas'   => $data['idaktivitas']
        ]);
    }
}