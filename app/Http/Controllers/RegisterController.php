<?php
    namespace App\Http\Controllers;

    use App\User;
    use App\UserDetail;
    use App\UserJurusan;
    use App\UserStatus;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Validator;

    class RegisterController extends Controller
    {
        public function index(Request $request){
            $data = $request->all();
            $data['iduser'] = $this->getID();
            $data['tanggalregistrasi'] = date("Y-m-d");
            $data['wakturegistrasi'] = date("h:i:s");
            $data['profesi'] = "";
            $data['perusahaan'] = "";
            $data['strata'] = "";
            $data['jurusan'] = "";
            $data['angkatan'] = "";
            $validate = $this->validator($data);
            $valUser  = $this->cekUser($data['email']);
            if(!$validate->fails() && $valUser == true){
                $this->create($data);
                $data['user_detail'] = UserDetail::where('iduser', $data['iduser'])->first();
                $res['success'] = true;
                $res['message'] = "Berhasil menambahkan user";
                $res['data']    = $data;
                return response($res,200);
            }else{
                $res['error'] = true;
                $res['message'] = "Isi form secara lengkap";
                if($valUser == false){
                    $res['message'] = "Email sudah terdaftar";
                }
                return response($res, 500);
            }

        }

        protected function cekUser($email){
            $count = count(User::where('email', $email)->get());
            if($count == 0){
                return true;
            }else{
                return false;
            }
        }

        protected function getID(){
            return DB::table('user')->max('iduser') + 1;
        }

        protected function validator(array $data){
            return Validator::make($data, [
                'email' => 'required',
                'fullname' => 'required|string',
                'password' => 'required',
            ]);
        }

        protected function create(array $data){
             User::create([
                'iduser' => $data['iduser'],
                'email' => $data['email'],
                'fullname' => $data['fullname'],
                'password' => $data['password'],
                'tanggalregistrasi' => $data['tanggalregistrasi'],
                'wakturegistrasi' => $data['wakturegistrasi']
             ]);

             UserDetail::create([
                 'iduser' => $data['iduser'],
                 'urlfotoprofile' => null,
                 'handphone' => null,
                 'lokasi' => null,
                 'bio' => null,
                 'profesi' => $data['profesi'],
                 'perusahaan' => $data['perusahaan']
             ]);

             UserStatus::create([
                 'iduser'   => $data['iduser'],
                 'status'   => 1
             ]);
        }
    }