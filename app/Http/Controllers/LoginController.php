<?php
namespace App\Http\Controllers;

use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function index(Request $request){
        $data['email'] = $request->get('email');
        $data['password'] = $request->get('password');
        return $this->login($data);
    }

    protected function login(array $data){
        $email = $data['email'];
        $password = $data['password'];
        $query = "SELECT user.iduser, urlfotoprofile, status FROM user
                INNER JOIN user_detail using(iduser)
                INNER JOIN user_status using(iduser)
                WHERE email = '$email' AND password = '$password'";
        $user = DB::select(DB::raw($query));
//        $res['query'] = $query;
        if(count($user) == 1){
            if($user[0]->status == 1){
                $res['success'] = true;
                $res['message'] = "Berhasil Login";
                $res['data'] = $user;
                return response($res,200);
            }else{
                $res['success'] = false;
                $res['message'] = "Maaf akun Anda sudah diblokir";
                $res['data'] = $user;
                return response($res,500);
            }
        }else{
            $res['error'] = true;
            $res['message'] = "Email/ Password yang anda masukkan salah";
            return response($res, 500);
        }
    }

    public function getProfile($idUser){
        $user = User::where('iduser','=', $idUser)->with('userDetail')->get();
        if(count($user) == 1){
            $res['success'] = true;
            $res['message'] = "Data User Tersedia";
            $res['data'] = $user;
            return response($res,200);
        }else{
            $res['error'] = true;
            $res['message'] = "Data user dengan id user tersebut tidak tersedia";
            return response($res, 500);
        }
    }

    public function updateProfile(Request $request){
        $idUser = $request->iduser;
        $user = User::where('iduser', $idUser);
        $userDetail = UserDetail::where('iduser', $idUser);

        $update = $user->update([
            'fullname'   => $request->fullname,
        ]);

        if($request->hasFile('urlfoto')){
            $image = $request->file('urlfoto');
            $filename = "user". $idUser . '.'.$image->getClientOriginalExtension();
            $done = $image->move(public_path("/upload/user/"), $filename);
            $url = url('/') . '/upload/user/' . $filename;
            $urlfoto = $url;
            $userDetail->update([
                'urlfotoprofile'    => $urlfoto,
            ]);
        }else{
            if($userDetail->first()->urlfotoprofile != null){
                $urlfoto = $userDetail->first()->urlfotoprofile;
            }else{
                $urlfoto = null;
            }
        }

        $userDetail->update([
            'handphone'         => $request->handphone,
            'lokasi'            => $request->lokasi,
            'bio'               => $request->bio
        ]);

        $data = $request->all();
        $data['urlfotoganti'] = $urlfoto;

        $res['success'] = true;
        $res['message'] = "Anda berhasil mengubah data profil";
        $res['data']    = $data;

        return response($res, 200);
    }

    public function cekBlock($idUser){
        $query = "SELECT iduser from user_status where iduser = '$idUser' AND status = 2";
        $data  = DB::select(DB::raw($query));
        if(count($data) == 1){
            return response("true", 200);
        }else{
            return response("false", 200);
        }
    }

    public function testingNotification($idUser, Request $request){
        // \OneSignal::sendNotificationUsingTags(
        //     "Testing push notification lewat api",
        //     array(
        //         ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $idUser]
        //     ),
        //     $url = null,
        //     $data = null,
        //     $buttons = null,
        //     $schedule = null
        // );
    }
}