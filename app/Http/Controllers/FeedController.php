<?php
namespace App\Http\Controllers;

use App\AksiAktivitasUsul;
use App\AksiUsulVote;
use App\UserAktivitas;
use App\UserAktivitasEvent;
use App\UserAktivitasUsul;
use App\UserRelasi;
use App\SubAksi;
use App\SubAksiDonasi;
use App\SubAksiVolunteer;
use App\UserGrupSubgrup;
use App\Aksi;
use App\AksiAktivitasResponUsul;
use Illuminate\Support\Facades\DB;
class FeedController extends Controller
{
    private function getLatestSubAksi($idaksi){
        $query = SubAksi::select('idsubaksi')->where('idaksi',$idaksi)->orderBy('idsubaksi','desc')->first();
        return $query->idsubaksi;
    }

    private function getTargetDonasi($idaksi,$idsubaksi){
        $query = SubAksiDonasi::select('targetdonasi')->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
        if($query != null){
            return $query->targetdonasi;
        }else{
            return $query;
        }

    }

    private function getTargetVolunteer($idaksi,$idsubaksi){
        $query = SubAksiVolunteer::select('targetvolunteer')->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
        if($query != null){
            return $query->targetvolunteer;
        } else {
            return $query;
        }
    }

    private function getDurasiDonasi($idaksi,$idsubaksi){
        $query = SubAksiDonasi::select(DB::raw('DATEDIFF(tanggalselesai, NOW()) as daysleft'))->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
        if($query != null) {
            $daysleft = $query->daysleft;
        } else {
            $daysleft = null;
        }
        return $daysleft;
    }

    private function getDurasiVolunteer($idaksi,$idsubaksi){
        $query = SubAksiVolunteer::select(DB::raw('DATEDIFF(tanggalselesai, NOW()) as daysleft'))->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
        if($query != null) {
            $daysleft = $query->daysleft;
        } else {
            $daysleft = null;
        }
        return $daysleft;
    }

    private function getAksiDonasiStatus($idaksi, $idsubaksi){
        $query = SubAksiDonasi::select('targetdonasi')->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
        if($query != null){
            return true;
        }else{
            return false;
        }
    }

    private function getAksiVolunteerStatus($idaksi, $idsubaksi){
        $query = SubAksiVolunteer::select('targetvolunteer')->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
        if($query != null){
            return true;
        } else {
            return false;
        }
    }

    private function getListAksi($grupIds){
        $aksi = Aksi::select('idaksi')->whereIn('idgrup', $grupIds)->where('status', 1)->get();
        return $aksi;
    }

    public function index($idUser, $offset=0, $limit=0){
        $grupIds = $this->getGrupId($idUser);
        $followerIds = $this->getFollowerId($idUser);
        $listAksi = $this->getListAksi($grupIds);

        $aktivitas = UserAktivitas::with('aksi.aksiFoto')->with('aksi.subAksi')->with('user.userDetail')->with('jenisAktivitas')
            ->whereIn('iduser', $followerIds)->whereIn('idaksi', $listAksi)->where('idjenisaktivitas','<>',11);
        if($limit != 0){
            $aktivitas = $aktivitas->offset($offset)->limit($limit);
        }
        $aktivitas = $aktivitas->orderBy('tanggalaktivitas','desc')->orderBy('waktuaktivitas','desc')->get();

        $event = UserAktivitasEvent::with('event.eventFoto')->with('user.userDetail')->with('jenisAktivitas')->whereIn('iduser', $followerIds);
        if($limit != 0){
            $event = $event->offset($offset)->limit($limit);
        }
        $event = $event->orderBy('tanggalaktivitas','desc')->orderBy('waktuaktivitas','desc')->get();

        $res['success'] = true;
        if(count($aktivitas) != 0){
            $res['total'] = count($aktivitas);
        }else{
            $res['total'] = 1;
        }
        $res['data'] = $aktivitas;
//         $i = 0;
//         foreach($event as $e){
//             $res['data'][$i] = $e;
//             $i++;
//         }
// //        $res['data2'] = $event;

        $i = 0;
        foreach($aktivitas as $act){
            $idAksi = $act->idaksi;
            $subAksiId = $this->getLatestSubAksi($idAksi);
            $jumlahDonasi = DB::select(DB::raw("SELECT SUM(nilaidonasi) AS total FROM user_aktivitas_donasi INNER JOIN sub_aksi_aktivitas_donasi USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
            $jumlahVolunteer = DB::select(DB::raw("SELECT COUNT(iduser) AS total FROM user_aktivitas_volunteer INNER JOIN sub_aksi_aktivitas_volunteer USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
            $targetDonasi = $this->getTargetDonasi($idAksi,$subAksiId);
            $targetVolunteer = $this->getTargetVolunteer($idAksi,$subAksiId);
            $durasiDonasi = $this->getDurasiDonasi($idAksi, $subAksiId);
            $durasiVolunteer = $this->getDurasiVolunteer($idAksi, $subAksiId);

//            $res['data'][$i]['idsubaksi'] = $subAksiId;
            $res['data'][$i]['totaldonasi'] = $jumlahDonasi[0]->total != null? $jumlahDonasi[0]->total: 0;
            $res['data'][$i]['targetdonasi'] = $targetDonasi;
            if($targetDonasi != null){
                $res['data'][$i]['persendonasi'] = ceil(($jumlahDonasi[0]->total/$targetDonasi)*100);
            } else {
                $res['data'][$i]['persendonasi'] = ceil(($jumlahDonasi[0]->total/1)*100);
            }
            $res['data'][$i]['totalvolunteer'] = $jumlahVolunteer[0]->total;
            $res['data'][$i]['targetvolunteer'] = $targetVolunteer;
            if($targetVolunteer != null){
                $res['data'][$i]['persenvolunteer'] = ceil(($jumlahVolunteer[0]->total/$targetVolunteer)*100);
            } else {
                $res['data'][$i]['persenvolunteer'] = ceil(($jumlahVolunteer[0]->total/1)*100);
            }
            $res['data'][$i]['durasidonasi'] = $durasiDonasi;
            $res['data'][$i]['durasivolunteer'] = $durasiVolunteer;
            $res['data'][$i]['statusdonasi'] = $this->getAksiDonasiStatus($idAksi, $subAksiId);
            $res['data'][$i]['statusvolunteer'] = $this->getAksiVolunteerStatus($idAksi, $subAksiId);

            $jenisAktivitas = $act->idjenisaktivitas;
            $idUserAktivitas = $act->iduser;
            $idAktivitas = $act->idaktivitas;
            if($jenisAktivitas == 5 || $jenisAktivitas == 4){
                $respon = UserAktivitasUsul::where('iduser', $idUserAktivitas)->where('idaktivitas', $idAktivitas);
                $res['data'][$i]['usulan'] = $respon->first();
                if($jenisAktivitas == 5) {
                    $idusulan = AksiAktivitasResponUsul::where('iduser', $idUserAktivitas)->where('idaktivitas', $idAktivitas)->first()->idusul;
                    $usulan = AksiAktivitasUsul::where('idusul', $idusulan)->where('idaksi', $idAksi)->first();
                    $userUsulan = $usulan->iduser;
                    $aktivitasUsulan = $usulan->idaktivitas;
                    $result = UserAktivitasUsul::where('iduser', $userUsulan)->where('idaktivitas', $aktivitasUsulan)->first();
                    $res['data'][$i]['usulan']['reference'] = $result;
                }
            }
            if($jenisAktivitas == 6){
                $idUsul = AksiUsulVote::where('iduservote', $idUserAktivitas)->where('idaksi', $idAksi)->first()->idusul;
                $usulan = AksiAktivitasUsul::where('idusul', $idUsul)->where('idaksi', $idAksi)->first();
                $userUsulan = $usulan->iduser;
                $aktivitasUsulan = $usulan->idaktivitas;
                $result = UserAktivitasUsul::where('iduser', $userUsulan)->where('idaktivitas', $aktivitasUsulan)->first();

                $likes  = count(AksiUsulVote::where('idaksi', $idAksi)->where('idusul', $idUsul)->get());
                $res['data'][$i]['usulan'] = $result;
                $res['data'][$i]['usulan']['likes'] = $likes;
            }
            $i++;
        }
        return response($res,200);
    }


    private function getGrupId($idUser){
        $query = UserGrupSubgrup::where('iduser',$idUser)->get();
        $data[0] = 0;
        if(count($query) != 0) {
            $i = 1;
            foreach ($query as $q) {
                $data[$i] = $q->idgrup;
                $i++;
            }
        }
        return $data;
    }

    private function getAksiId($idGrup){
        $query = Aksi::whereIn('idgrup',$idGrup)->get();
        if(count($query) != 0){
            $i=0;
            foreach($query as $q){
                $data[$i] = $q->idaksi;
                $i++;
            }
        }else{
            $data = [];
        }
        return $data;
    }

    private function getFollowerId($id){
        $followings = UserRelasi::where('iduser','=', $id)->get();
        if(count($followings) != 0) {
            $i = 0;
            foreach ($followings as $following) {
                $data[$i] = $following->iduser_following;
                $i++;
            }
        }else{
            $data = [];
        }
        return $data;
    }

    public function myActivity($idUser, $offset, $limit){
        $listAksi = Aksi::select('idaksi')->where('status', 1)->get();
        $aktivitas = UserAktivitas::with('aksi.aksiFoto')->with('aksi.subAksi')->with('user.userDetail')->with('jenisAktivitas')
            ->where('iduser', $idUser)->where('idjenisaktivitas','<>',11)->whereIn('idaksi', $listAksi);
        if($limit != 0){
            $aktivitas = $aktivitas->offset($offset)->limit($limit);
        }
        $aktivitas = $aktivitas->orderBy('tanggalaktivitas','desc')->orderBy('waktuaktivitas','desc')->get();

        $event = UserAktivitasEvent::with('event.eventFoto')->with('user.userDetail')->with('jenisAktivitas')->where('iduser', $idUser);
        if($limit != 0){
            $event = $event->offset($offset)->limit($limit);
        }
        $event = $event->orderBy('tanggalaktivitas','desc')->orderBy('waktuaktivitas','desc')->get();

        $res['success'] = true;

        if(count($aktivitas) != 0){
            $res['total'] = count($aktivitas);
        }else{
            $res['total'] = 1;
        }

        $res['data'] = $aktivitas;
        $res['data2'] = $event;

        $i = 0;
        foreach($aktivitas as $act){
            $idAksi = $act->idaksi;
            $subAksiId = $this->getLatestSubAksi($idAksi);
            $jumlahDonasi = DB::select(DB::raw("SELECT SUM(nilaidonasi) AS total FROM user_aktivitas_donasi INNER JOIN sub_aksi_aktivitas_donasi USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
            $jumlahVolunteer = DB::select(DB::raw("SELECT COUNT(iduser) AS total FROM user_aktivitas_volunteer INNER JOIN sub_aksi_aktivitas_volunteer USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
            $targetDonasi = $this->getTargetDonasi($idAksi,$subAksiId);
            $targetVolunteer = $this->getTargetVolunteer($idAksi,$subAksiId);
            $durasiDonasi = $this->getDurasiDonasi($idAksi, $subAksiId);
            $durasiVolunteer = $this->getDurasiVolunteer($idAksi, $subAksiId);

//            $res['data'][$i]['idsubaksi'] = $subAksiId;
            $res['data'][$i]['totaldonasi'] = $jumlahDonasi[0]->total != null? $jumlahDonasi[0]->total: 0;
            $res['data'][$i]['targetdonasi'] = $targetDonasi;
            if($targetDonasi != null){
                $res['data'][$i]['persendonasi'] = ceil(($jumlahDonasi[0]->total/$targetDonasi)*100);
            } else {
                $res['data'][$i]['persendonasi'] = ceil(($jumlahDonasi[0]->total/1)*100);
            }
            $res['data'][$i]['totalvolunteer'] = $jumlahVolunteer[0]->total;
            $res['data'][$i]['targetvolunteer'] = $targetVolunteer;
            if($targetVolunteer != null){
                $res['data'][$i]['persenvolunteer'] = ceil(($jumlahVolunteer[0]->total/$targetVolunteer)*100);
            } else {
                $res['data'][$i]['persenvolunteer'] = ceil(($jumlahVolunteer[0]->total/1)*100);
            }
            $res['data'][$i]['durasidonasi'] = $durasiDonasi;
            $res['data'][$i]['durasivolunteer'] = $durasiVolunteer;
            $res['data'][$i]['statusdonasi'] = $this->getAksiDonasiStatus($idAksi, $subAksiId);
            $res['data'][$i]['statusvolunteer'] = $this->getAksiVolunteerStatus($idAksi, $subAksiId);

            $jenisAktivitas = $act->idjenisaktivitas;
            $idUserAktivitas = $act->iduser;
            $idAktivitas = $act->idaktivitas;
            if($jenisAktivitas == 5 || $jenisAktivitas == 4){
                $respon = UserAktivitasUsul::where('iduser', $idUserAktivitas)->where('idaktivitas', $idAktivitas);
                $res['data'][$i]['usulan'] = $respon->first();
                if($jenisAktivitas == 5) {
                    $idusulan = AksiAktivitasResponUsul::where('iduser', $idUserAktivitas)->where('idaktivitas', $idAktivitas)->first()->idusul;
                    $usulan = AksiAktivitasUsul::where('idusul', $idusulan)->where('idaksi', $idAksi)->first();
                    $userUsulan = $usulan->iduser;
                    $aktivitasUsulan = $usulan->idaktivitas;
                    $result = UserAktivitasUsul::where('iduser', $userUsulan)->where('idaktivitas', $aktivitasUsulan)->first();
                    $res['data'][$i]['usulan']['reference'] = $result;
                }
            }
            if($jenisAktivitas == 6){
                $idUsul = AksiUsulVote::where('iduservote', $idUserAktivitas)->where('idaksi', $idAksi)->first()->idusul;
                $usulan = AksiAktivitasUsul::where('idusul', $idUsul)->where('idaksi', $idAksi)->first();
                $userUsulan = $usulan->iduser;
                $aktivitasUsulan = $usulan->idaktivitas;
                $result = UserAktivitasUsul::where('iduser', $userUsulan)->where('idaktivitas', $aktivitasUsulan)->first();

                $likes  = count(AksiUsulVote::where('idaksi', $idAksi)->where('idusul', $idUsul)->get());
                $res['data'][$i]['usulan'] = $result;
                $res['data'][$i]['usulan']['likes'] = $likes;
            }
            $i++;
        }
        return response($res,200);
    }
}