<?php
namespace App\Http\Controllers;


use App\Aksi;
use App\UserGrupSubgrup;
use App\SubAksi;
use App\SubAksiDonasi;
use App\SubAksiVolunteer;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class AksiController extends Controller
{
    private function getGroupsId($id){
        $grups = UserGrupSubgrup::where('iduser','=',$id)->get();
        $i = 1;
        $groups[0] = 0;
        foreach($grups as $grup){
            $groups[$i] = $grup->idgrup;
            $i++;
        }
        return $groups;
    }

    private function getJenisAksi($idAksi){
        return SubAksi::select('idjenisaksi')->where('idaksi', $idAksi)->where('idsubaksi', 0)->first();
    }

    public function topAksi($idUser){
        $aksi = DB::select(DB::raw("SELECT idaksi,namaaksi,idfoto,urlfotoaksi,iduser_initiator,fullname,jumlah,tipeaksi,tanggalpembuatan,waktupembuatan, status
                FROM
                (
                    SELECT idaksi,namaaksi,iduser_initiator,fullname,jumlah,tipeaksi,tanggalpembuatan,waktupembuatan, status
                    FROM
                    (
                        SELECT aksi.idaksi AS idaksi,namaaksi,iduser_initiator,jumlah, tipeaksi,tanggalpembuatan,waktupembuatan, status
                
                        FROM `aksi`
                
                        INNER JOIN
                        (
                            SELECT idaksi,count(*) AS jumlah FROM `user_aktivitas`
                            WHERE 
                
                            idaksi IN 
                            (
                                SELECT idaksi FROM aksi 
                                WHERE 
                                (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                )
                
                                OR 
                                (
                                    idgrup IN 
                                    (
                                        SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                    )
                                    AND idsubgrup='0'
                                )
                
                                OR (idgrup='0' AND idsubgrup='0')
                            )
                
                            GROUP BY idaksi
                        ) AS proses
                
                        WHERE aksi.idaksi = proses.idaksi
                
                        ORDER BY jumlah DESC
                    ) AS proses2
                
                    INNER JOIN user
                
                    WHERE user.iduser = proses2.iduser_initiator
                ) AS proses3
                
                INNER JOIN
                
                aksi_foto USING (idaksi) WHERE proses3.tipeaksi <> 1 AND proses3.status = 1"));
        $res['success'] = true;
        $res['data'] = $aksi;
        $i = 0;
        foreach($aksi as $a){
            $res['data'][$i]->jenisaksi = $this->getJenisAksi($a->idaksi);
            $i++;
        }
        return response($res,200);
    }

    public function newAksi($idUser){
        $aksi = DB::select(
            DB::raw("SELECT idaksi,namaaksi,idfoto,urlfotoaksi,tanggalpembuatan,waktupembuatan,iduser_initiator,fullname,tipeaksi, status FROM
                        (
                            SELECT idaksi,namaaksi,tanggalpembuatan,waktupembuatan,iduser_initiator,tipeaksi, status FROM aksi
                            WHERE
                            (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                OR 
                                (
                                idgrup IN
                                (
                                    SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                           AND idsubgrup='0'
                                )
                        
                                OR (idgrup='0' AND idsubgrup='0')
                        ) AS proses
                        
                        INNER JOIN
                        
                        aksi_foto USING (idaksi)
                        
                        INNER JOIN
                        
                        user WHERE user.iduser = proses.iduser_initiator AND tipeaksi <> 1 AND status = 1
                        
                        ORDER BY tanggalpembuatan DESC, waktupembuatan DESC"));
        $res['success'] = true;
        $res['total'] = count($aksi);
        $res['data'] = $aksi;
        $i = 0;
        foreach($aksi as $a){
            $res['data'][$i]->jenisaksi = $this->getJenisAksi($a->idaksi);
            $i++;
        }
        return response($res,200);
    }

    public function highlightAksi($idUser){
        $aksi = DB::select(
            DB::raw("SELECT idaksi,namaaksi,idfoto,urlfotoaksi,tanggalpembuatan,waktupembuatan,iduser_initiator,fullname,tipeaksi, status FROM
                        (
                            SELECT idaksi,namaaksi,tanggalpembuatan,waktupembuatan,iduser_initiator,tipeaksi, status FROM aksi
                            WHERE
                            (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                OR 
                                (
                                idgrup IN
                                (
                                    SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                           AND idsubgrup='0'
                                )
                        
                                OR (idgrup='0' AND idsubgrup='0')
                        ) AS proses
                        
                        INNER JOIN
                        
                        aksi_foto USING (idaksi)
                        
                        INNER JOIN
                        
                        user WHERE user.iduser = proses.iduser_initiator AND tipeaksi <> 1 AND proses.iduser_initiator = 0 AND status = 1
                        
                        ORDER BY tanggalpembuatan DESC, waktupembuatan DESC"));
        $res['success'] = true;
        $res['total'] = count($aksi);
        $res['data'] = $aksi;
        $i = 0;
        foreach($aksi as $a){
            $res['data'][$i]->jenisaksi = $this->getJenisAksi($a->idaksi);
            $i++;
        }
        return response($res,200);
    }

    public function searchTopAksi(Request $request){
        $idUser = $request->iduser;
        $search = $request->q;

        $aksi = DB::select(DB::raw("SELECT idaksi,namaaksi,idfoto,urlfotoaksi,iduser_initiator,fullname,jumlah,tipeaksi,tanggalpembuatan,waktupembuatan, status
                FROM
                (
                    SELECT idaksi,namaaksi,iduser_initiator,fullname,jumlah,tipeaksi,tanggalpembuatan,waktupembuatan, status
                    FROM
                    (
                        SELECT aksi.idaksi AS idaksi,namaaksi,iduser_initiator,jumlah, tipeaksi,tanggalpembuatan,waktupembuatan, status
                
                        FROM `aksi`
                
                        INNER JOIN
                        (
                            SELECT idaksi,count(*) AS jumlah FROM `user_aktivitas`
                            WHERE 
                
                            idaksi IN 
                            (
                                SELECT idaksi FROM aksi 
                                WHERE 
                                (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                )
                
                                OR 
                                (
                                    idgrup IN 
                                    (
                                        SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                    )
                                    AND idsubgrup='0'
                                )
                
                                OR (idgrup='0' AND idsubgrup='0')
                            )
                
                            GROUP BY idaksi
                        ) AS proses
                
                        WHERE aksi.idaksi = proses.idaksi
                
                        ORDER BY jumlah DESC
                    ) AS proses2
                
                    INNER JOIN user
                
                    WHERE user.iduser = proses2.iduser_initiator
                ) AS proses3
                
                INNER JOIN
                
                aksi_foto USING (idaksi) WHERE proses3.namaaksi LIKE '%$search%' AND proses3.tipeaksi <> 1 AND proses3.status = 1"));
        $res['success'] = true;
        $res['data'] = $aksi;
        $i = 0;
        foreach($aksi as $a){
            $res['data'][$i]->jenisaksi = $this->getJenisAksi($a->idaksi);
            $i++;
        }
        return response($res,200);
    }

    public function searchNewAksi(Request $request){
        $idUser = $request->iduser;
        $search = $request->q;
        $aksi = DB::select(
            DB::raw("SELECT idaksi,namaaksi,idfoto,urlfotoaksi,tanggalpembuatan,waktupembuatan,iduser_initiator,fullname,tipeaksi, status FROM
                        (
                            SELECT idaksi,namaaksi,tanggalpembuatan,waktupembuatan,iduser_initiator,tipeaksi, status FROM aksi
                            WHERE
                            (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                OR 
                                (
                                idgrup IN
                                (
                                    SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                           AND idsubgrup='0'
                                )
                        
                                OR (idgrup='0' AND idsubgrup='0')
                        ) AS proses
                        
                        INNER JOIN
                        
                        aksi_foto USING (idaksi)
                        
                        INNER JOIN
                        
                        user WHERE user.iduser = proses.iduser_initiator AND proses.namaaksi LIKE '%$search%' AND tipeaksi <> 1 AND status = 1
                        
                        ORDER BY tanggalpembuatan DESC, waktupembuatan DESC"));
        $res['success'] = true;
        $res['total'] = count($aksi);
        $res['data'] = $aksi;
        $i = 0;
        foreach($aksi as $a){
            $res['data'][$i]->jenisaksi = $this->getJenisAksi($a->idaksi);
            $i++;
        }
        return response($res,200);
    }

    public function searchHighlightAksi(Request $request){
        $idUser = $request->iduser;
        $search = $request->q;
        $aksi = DB::select(
            DB::raw("SELECT idaksi,namaaksi,idfoto,urlfotoaksi,tanggalpembuatan,waktupembuatan,iduser_initiator,fullname,tipeaksi, status FROM
                        (
                            SELECT idaksi,namaaksi,tanggalpembuatan,waktupembuatan,iduser_initiator,tipeaksi, status FROM aksi
                            WHERE
                            (idgrup,idsubgrup) IN
                                (
                                    SELECT idgrup,idsubgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                OR 
                                (
                                idgrup IN
                                (
                                    SELECT idgrup FROM user_grup_subgrup WHERE iduser='$idUser'
                                     )
                                           AND idsubgrup='0'
                                )
                        
                                OR (idgrup='0' AND idsubgrup='0')
                        ) AS proses
                        
                        INNER JOIN
                        
                        aksi_foto USING (idaksi)
                        
                        INNER JOIN
                        
                        user WHERE user.iduser = proses.iduser_initiator AND tipeaksi <> 1 AND proses.iduser_initiator = 0 AND proses.namaaksi LIKE '%$search%' AND status = 1
                        
                        ORDER BY tanggalpembuatan DESC, waktupembuatan DESC"));
        $res['success'] = true;
        $res['total'] = count($aksi);
        $res['data'] = $aksi;
        $i = 0;
        foreach($aksi as $a){
            $res['data'][$i]->jenisaksi = $this->getJenisAksi($a->idaksi);
            $i++;
        }
        return response($res,200);
    }
}