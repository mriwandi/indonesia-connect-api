<?php
    namespace App\Http\Controllers;
    use App\AksiAktivitasResponUsul;
    use App\AksiAktivitasUsul;
    use App\AksiUsulVote;
    use App\UserAktivitasEvent;
    use App\UserAktivitasUsul;
    use Illuminate\Http\Request;
    use App\Grup;
    use App\GrupDetail;
    use App\SubGrup;
    use App\UserGrupSubgrup;
    use App\UserAktivitas;
    use App\Aksi;
    use App\SubAksi;
    use App\SubAksiDonasi;
    use App\SubAksiVolunteer;
    use Illuminate\Support\Facades\DB;
    class GroupController extends Controller
    {
        public function listGrup($idUser){
            $query = UserGrupSubgrup::where('iduser',$idUser)->with('grup.grupDetail')->get();

            $res['success'] = true;
            $res['message'] = "Grup tersedia";
            $res['data'] = $query;

            return response($res, 200);
        }

        public function listGrupCreated($idUser){
            $query = Grup::where('iduserinitiator',$idUser)->with('grupDetail')->get();

            $res['success'] = true;
            $res['message'] = "Grup tersedia";
            $res['data'] = $query;

            return response($res, 200);
        }

        private function getLatestSubAksi($idaksi){
            $query = SubAksi::select('idsubaksi')->where('idaksi',$idaksi)->orderBy('idsubaksi','desc')->first();
            return $query->idsubaksi;
        }

        private function getTargetDonasi($idaksi,$idsubaksi){
            $query = SubAksiDonasi::select('targetdonasi')->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
            if($query != null){
                return $query->targetdonasi;
            }else{
                return $query;
            }

        }

        private function getTargetVolunteer($idaksi,$idsubaksi){
            $query = SubAksiVolunteer::select('targetvolunteer')->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
            if($query != null){
                return $query->targetvolunteer;
            } else {
                return $query;
            }
        }

        private function getDurasiDonasi($idaksi,$idsubaksi){
            $query = SubAksiDonasi::select(DB::raw('DATEDIFF(tanggalselesai, tanggalmulai) as daysleft'))->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
            if($query != null) {
                $daysleft = $query->daysleft;
            } else {
                $daysleft = null;
            }
            return $daysleft;
        }

        private function getDurasiVolunteer($idaksi,$idsubaksi){
            $query = SubAksiVolunteer::select(DB::raw('DATEDIFF(tanggalselesai, tanggalmulai) as daysleft'))->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
            if($query != null) {
                $daysleft = $query->daysleft;
            } else {
                $daysleft = null;
            }
            return $daysleft;
        }

        private function getAksiDonasiStatus($idaksi, $idsubaksi){
            $query = SubAksiDonasi::select('targetdonasi')->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
            if($query != null){
                return true;
            }else{
                return false;
            }
        }

        private function getAksiVolunteerStatus($idaksi, $idsubaksi){
            $query = SubAksiVolunteer::select('targetvolunteer')->where('idaksi',$idaksi)->where('idsubaksi',$idsubaksi)->first();
            if($query != null){
                return true;
            } else {
                return false;
            }
        }

        private function getListAksi($grupIds){
            $aksi = Aksi::select('idaksi')->whereIn('idgrup', $grupIds)->where('status', 1)->get();
            return $aksi;
        }

        public function feeds($idGrup){
            $grupIds = [$idGrup];
            $listAksi = $this->getListAksi($grupIds);

            $aktivitas = UserAktivitas::with('aksi.aksiFoto')->with('aksi.subAksi')->with('user.userDetail')->with('jenisAktivitas')->whereIn('idaksi', $listAksi)->where('idjenisaktivitas', 1);
//            if($limit != 0){
//                $aktivitas = $aktivitas->offset($offset)->limit($limit);
//            }
            $aktivitas = $aktivitas->orderBy('tanggalaktivitas','desc')->orderBy('waktuaktivitas','desc')->get();

            $event = UserAktivitasEvent::with('event.eventFoto')->with('user.userDetail')->with('jenisAktivitas')->where('idjenisaktivitas', 7);
//            if($limit != 0){
//                $event = $event->offset($offset)->limit($limit);
//            }
            $event = $event->orderBy('tanggalaktivitas','desc')->orderBy('waktuaktivitas','desc')->get();

            $res['success'] = true;
            $res['total'] = count($aktivitas);
            $res['data'] = $aktivitas;
            $res['data2'] = $event;

            $i = 0;
            foreach($aktivitas as $act){
                $idAksi = $act->idaksi;
                $subAksiId = $this->getLatestSubAksi($idAksi);
                $jumlahDonasi = DB::select(DB::raw("SELECT SUM(nilaidonasi) AS total FROM user_aktivitas_donasi INNER JOIN sub_aksi_aktivitas_donasi USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
                $jumlahVolunteer = DB::select(DB::raw("SELECT COUNT(iduser) AS total FROM user_aktivitas_volunteer INNER JOIN sub_aksi_aktivitas_volunteer USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
                $targetDonasi = $this->getTargetDonasi($idAksi,$subAksiId);
                $targetVolunteer = $this->getTargetVolunteer($idAksi,$subAksiId);
                $durasiDonasi = $this->getDurasiDonasi($idAksi, $subAksiId);
                $durasiVolunteer = $this->getDurasiVolunteer($idAksi, $subAksiId);

//            $res['data'][$i]['idsubaksi'] = $subAksiId;
                $res['data'][$i]['totaldonasi'] = $jumlahDonasi[0]->total != null? $jumlahDonasi[0]->total: 0;
                $res['data'][$i]['targetdonasi'] = $targetDonasi;
                if($targetDonasi != null){
                    $res['data'][$i]['persendonasi'] = ceil(($jumlahDonasi[0]->total/$targetDonasi)*100);
                } else {
                    $res['data'][$i]['persendonasi'] = ceil(($jumlahDonasi[0]->total/1)*100);
                }
                $res['data'][$i]['totalvolunteer'] = $jumlahVolunteer[0]->total;
                $res['data'][$i]['targetvolunteer'] = $targetVolunteer;
                if($targetVolunteer != null){
                    $res['data'][$i]['persenvolunteer'] = ceil(($jumlahVolunteer[0]->total/$targetVolunteer)*100);
                } else {
                    $res['data'][$i]['persenvolunteer'] = ceil(($jumlahVolunteer[0]->total/1)*100);
                }
                $res['data'][$i]['durasidonasi'] = $durasiDonasi;
                $res['data'][$i]['durasivolunteer'] = $durasiVolunteer;
                $res['data'][$i]['statusdonasi'] = $this->getAksiDonasiStatus($idAksi, $subAksiId);
                $res['data'][$i]['statusvolunteer'] = $this->getAksiVolunteerStatus($idAksi, $subAksiId);

                $jenisAktivitas = $act->idjenisaktivitas;
                $idUserAktivitas = $act->iduser;
                $idAktivitas = $act->idaktivitas;
                if($jenisAktivitas == 5 || $jenisAktivitas == 4){
                    $respon = UserAktivitasUsul::where('iduser', $idUserAktivitas)->where('idaktivitas', $idAktivitas);
                    $res['data'][$i]['usulan'] = $respon->first();
                    if($jenisAktivitas == 5) {
                        $idusulan = AksiAktivitasResponUsul::where('iduser', $idUserAktivitas)->where('idaktivitas', $idAktivitas)->first()->idusul;
                        $usulan = AksiAktivitasUsul::where('idusul', $idusulan)->where('idaksi', $idAksi)->first();
                        $userUsulan = $usulan->iduser;
                        $aktivitasUsulan = $usulan->idaktivitas;
                        $result = UserAktivitasUsul::where('iduser', $userUsulan)->where('idaktivitas', $aktivitasUsulan)->first();
                        $res['data'][$i]['usulan']['reference'] = $result;
                    }
                }
                if($jenisAktivitas == 6){
                    $idUsul = AksiUsulVote::where('iduservote', $idUserAktivitas)->where('idaksi', $idAksi)->first()->idusul;
                    $usulan = AksiAktivitasUsul::where('idusul', $idUsul)->where('idaksi', $idAksi)->first();
                    $userUsulan = $usulan->iduser;
                    $aktivitasUsulan = $usulan->idaktivitas;
                    $result = UserAktivitasUsul::where('iduser', $userUsulan)->where('idaktivitas', $aktivitasUsulan)->first();

                    $likes  = count(AksiUsulVote::where('idaksi', $idAksi)->where('idusul', $idUsul)->get());
                    $res['data'][$i]['usulan'] = $result;
                    $res['data'][$i]['usulan']['likes'] = $likes;
                }
                $i++;
            }
            return response($res,200);
        }

        public function memberFeeds($idGrup){
            $grupIds = [$idGrup, 0];
            $listAksi = $this->getListAksi($grupIds);
            $listMember = $this->getListIdMember($grupIds);

            $aktivitas = UserAktivitas::with('aksi.aksiFoto')->with('aksi.subAksi')->with('user.userDetail')->with('jenisAktivitas')->whereIn('idaksi', $listAksi)->whereIn('iduser', $listMember);
//            if($limit != 0){
//                $aktivitas = $aktivitas->offset($offset)->limit($limit);
//            }
            $aktivitas = $aktivitas->offset(0)->limit(10);
            $aktivitas = $aktivitas->orderBy('tanggalaktivitas','desc')->orderBy('waktuaktivitas','desc')->get();

            $event = UserAktivitasEvent::with('event.eventFoto')->with('user.userDetail')->with('jenisAktivitas');
//            if($limit != 0){
//                $event = $event->offset($offset)->limit($limit);
//            }
            $event = $event->orderBy('tanggalaktivitas','desc')->orderBy('waktuaktivitas','desc')->get();

            $res['success'] = true;
            $res['total'] = count($aktivitas);
            $res['data'] = $aktivitas;
            $res['data2'] = $event;

            $i = 0;
            foreach($aktivitas as $act){
                $idAksi = $act->idaksi;
                $subAksiId = $this->getLatestSubAksi($idAksi);
                $jumlahDonasi = DB::select(DB::raw("SELECT SUM(nilaidonasi) AS total FROM user_aktivitas_donasi INNER JOIN sub_aksi_aktivitas_donasi USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
                $jumlahVolunteer = DB::select(DB::raw("SELECT COUNT(iduser) AS total FROM user_aktivitas_volunteer INNER JOIN sub_aksi_aktivitas_volunteer USING(iduser,idaktivitas) WHERE idaksi='".$idAksi."' AND idsubaksi='".$subAksiId."'"));
                $targetDonasi = $this->getTargetDonasi($idAksi,$subAksiId);
                $targetVolunteer = $this->getTargetVolunteer($idAksi,$subAksiId);
                $durasiDonasi = $this->getDurasiDonasi($idAksi, $subAksiId);
                $durasiVolunteer = $this->getDurasiVolunteer($idAksi, $subAksiId);

//            $res['data'][$i]['idsubaksi'] = $subAksiId;
                $res['data'][$i]['totaldonasi'] = $jumlahDonasi[0]->total != null? $jumlahDonasi[0]->total: 0;
                $res['data'][$i]['targetdonasi'] = $targetDonasi;
                if($targetDonasi != null){
                    $res['data'][$i]['persendonasi'] = ceil(($jumlahDonasi[0]->total/$targetDonasi)*100);
                } else {
                    $res['data'][$i]['persendonasi'] = ceil(($jumlahDonasi[0]->total/1)*100);
                }
                $res['data'][$i]['totalvolunteer'] = $jumlahVolunteer[0]->total;
                $res['data'][$i]['targetvolunteer'] = $targetVolunteer;
                if($targetVolunteer != null){
                    $res['data'][$i]['persenvolunteer'] = ceil(($jumlahVolunteer[0]->total/$targetVolunteer)*100);
                } else {
                    $res['data'][$i]['persenvolunteer'] = ceil(($jumlahVolunteer[0]->total/1)*100);
                }
                $res['data'][$i]['durasidonasi'] = $durasiDonasi;
                $res['data'][$i]['durasivolunteer'] = $durasiVolunteer;
                $res['data'][$i]['statusdonasi'] = $this->getAksiDonasiStatus($idAksi, $subAksiId);
                $res['data'][$i]['statusvolunteer'] = $this->getAksiVolunteerStatus($idAksi, $subAksiId);

                $jenisAktivitas = $act->idjenisaktivitas;
                $idUserAktivitas = $act->iduser;
                $idAktivitas = $act->idaktivitas;
                if($jenisAktivitas == 5 || $jenisAktivitas == 4){
                    $respon = UserAktivitasUsul::where('iduser', $idUserAktivitas)->where('idaktivitas', $idAktivitas);
                    $res['data'][$i]['usulan'] = $respon->first();
                    if($jenisAktivitas == 5) {
                        $idusulan = AksiAktivitasResponUsul::where('iduser', $idUserAktivitas)->where('idaktivitas', $idAktivitas)->first()->idusul;
                        $usulan = AksiAktivitasUsul::where('idusul', $idusulan)->where('idaksi', $idAksi)->first();
                        $userUsulan = $usulan->iduser;
                        $aktivitasUsulan = $usulan->idaktivitas;
                        $result = UserAktivitasUsul::where('iduser', $userUsulan)->where('idaktivitas', $aktivitasUsulan)->first();
                        $res['data'][$i]['usulan']['reference'] = $result;
                    }
                }
                if($jenisAktivitas == 6){
                    $idUsul = AksiUsulVote::where('iduservote', $idUserAktivitas)->where('idaksi', $idAksi)->first()->idusul;
                    $usulan = AksiAktivitasUsul::where('idusul', $idUsul)->where('idaksi', $idAksi)->first();
                    $userUsulan = $usulan->iduser;
                    $aktivitasUsulan = $usulan->idaktivitas;
                    $result = UserAktivitasUsul::where('iduser', $userUsulan)->where('idaktivitas', $aktivitasUsulan)->first();

                    $likes  = count(AksiUsulVote::where('idaksi', $idAksi)->where('idusul', $idUsul)->get());
                    $res['data'][$i]['usulan'] = $result;
                    $res['data'][$i]['usulan']['likes'] = $likes;
                }
                $i++;
            }
            return response($res,200);
        }


        private function isAdmin($idGrup, $idUser){
            $query = Grup::where('idgrup', $idGrup)->where('iduserinitiator', $idUser)->first();
            if($query != null){
                return true;
            }else{
                return false;
            }
        }

        private function isJoined($idGrup, $idUser){
            $query = UserGrupSubgrup::where('idgrup', $idGrup)->where('iduser', $idUser)->first();
            if($query != null){
                return true;
            }else{
                return false;
            }
        }

        private function getLatestId(){
            $query = Grup::select('idgrup')->orderBy('idgrup', 'asc')->get();
            if($query != null){
                return count($query);
            }else{
                return '';
            }
        }

        public function detail($idGrup, $idUser){
            $query = DB::select("SELECT * FROM grup INNER JOIN grup_detail USING(idgrup) WHERE idgrup = $idGrup");
            $res['success'] = true;
            $res['message'] = "Grup tersedia";
            $res['data']    = json_decode(json_encode($query[0]), true);

            $res['data']['isAdmin']  = $this->isAdmin($idGrup, $idUser);
            $res['data']['isJoined'] = $this->isJoined($idGrup, $idUser);

            return response($res, 200);
        }

        public function create(Request $request){
            $id = $this->getLatestId();
            Grup::create([
                'idgrup'            => $id,
                'namagrup'          => $request->namagrup,
                'iduserinitiator'   => $request->iduserinitiator,
                'keterangan'        => ''
            ]);

            SubGrup::create([
               'idgrup'             => $id,
                'idsubgrup'         => 0,
                'namasubgrup'       => '',
                'iduserinitiator'   => $request->iduserinitiator,
                'keterangan'        => ''
            ]);

            if($request->hasFile('urlfoto')){
                $image = $request->file('urlfoto');
                $filename = "grup". $id . '.'.$image->getClientOriginalExtension();
                $image->move(public_path("/upload/grup/"), $filename);
                $url = url('/') . '/upload/grup/' . $filename;
                $urlfoto = $url;
            }else{
                $urlfoto = null;
            }

            GrupDetail::create([
                'idgrup'            => $id,
                'urlfoto'           => $urlfoto,
                'deskripsi'         => $request->deskripsi
            ]);

            $success = UserGrupSubgrup::create([
               'iduser'     => $request->iduserinitiator,
               'idgrup'     => $id,
               'idsubgrup'  => '0'
            ]);

            $res['success'] = true;
            $res['message'] = "Grup Created";
            $res['data'] = $request->all();
            $res['data']['idgrup'] = $id;

            return response($res, 200);
        }

        public function listMember($idGrup){
            $query = UserGrupSubgrup::where('idgrup', $idGrup)->with('user')->get();

            $res['success'] = true;
            $res['message'] = "List Member tersedia";
            $res['total'] = count($query);
            $res['data'] = $query;

            return response($res, 200);
        }

        public function getListIdMember($idGrup){
            $query = UserGrupSubgrup::select('iduser')->where('idgrup', $idGrup)->get();
            return $query;
        }

        public function join(Request $request){
            $data = $request->all();
            $idUser = $request->iduser;
            $idGrup = $request->idgrup;

            UserGrupSubgrup::create([
                'iduser'    => $idUser,
                'idgrup'    => $idGrup,
                'idsubgrup' => 0
            ]);

            $res['success'] = true;
            $res['message'] = "Anda bergabung dengan grup";
            $res['data'] = $data;

            return response($res, 200);
        }

        public function keluar(Request $request){
            $data = $request->all();
            $idUser = $request->iduser;
            $idGrup = $request->idgrup;

            $query = UserGrupSubgrup::where('iduser',$idUser)->where('idgrup', $idGrup);
            $query->delete();

            $res['success'] = true;
            $res['message'] = "Anda keluar dari grup";
            $res['data'] = $data;

            return response($res, 200);
        }

        public function updateGrup(Request $request){
            $idGrup = $request->idgrup;
            $grup = Grup::where('idgrup', $idGrup);
            $grupDetail = grupDetail::where('idgrup', $idGrup);

            $update = $grup->update([
               'namagrup'   => $request->namagrup,
            ]);

            if($request->hasFile('urlfoto')){
                $image = $request->file('urlfoto');
                $filename = "grup". $idGrup . '.'.$image->getClientOriginalExtension();
                $image->move(public_path("/upload/grup/"), $filename);
                $url = url('/') . '/upload/grup/' . $filename;
                $urlfoto = $url;
            }else{
                $urlfoto = null;
            }



            $grupDetail->update([
                'urlfoto'   => $urlfoto,
                'deskripsi' => $request->deskripsi
            ]);

            $res['success'] = true;
            $res['message'] = "Anda berhasil mengubah data grup";
            $res['data'] = $request->all();

            return response($res, 200);
        }

        public function search(Request $request){
            $q = $request->q;
            $query = Grup::with('grupDetail')->where('namagrup','like','%' . $q . '%')->where('namagrup','!=','0')->get();

            $res['success'] = true;
            $res['message'] = "List data grup";
            $res['data']    = $query;

            return response($res, 200);
        }
    }