<?php
    namespace App\Http\Controllers;
    use App\SubAksiAktivitasDonasi;
    use App\SubAksiAktivitasDonasiProses;
    use App\UserAktivitas;
    use App\UserAktivitasDonasi;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Mail;
    use Veritrans_Config;
    use Veritrans_VtWeb;
    use Veritrans_VtDirect;
    class PaymentGatewayController extends Controller
    {

        private function generatePaymentID($idaksi, $idsubaksi, $iduser){
            return count(SubAksiAktivitasDonasiProses::where([
                'idaksi'=> $idaksi,
                'idsubaksi' => $idsubaksi,
                'iduser' => $iduser
            ])->get()) + 1;
        }
        public function test(Request $request){
            //Config production key
            //            Veritrans_Config::$serverKey = 'Mid-server-iyD3XmgfzjGHlYC10AztEpQx';
            //            Veritrans_Config::$clientKey = 'Mid-client-By_0ZhzOpNGhQsw1';
            //            Veritrans_Config::$isProduction = false;
            //            Veritrans_Config::$isSanitized = false;

            //Config sandbox key
            Veritrans_Config::$clientKey = 'SB-Mid-client-NyUH0f3so3Gviwq4';
            Veritrans_Config::$serverKey = 'SB-Mid-server-nZSC_Mpn-aGj6ZZeCCOmL4sA';

            $transaction_details = array(
                'order_id' => time(),
                'gross_amount' => $request->besaran, // no decimal allowed for creditcard
            );

            $items = array(
                array(
                    'idaksi'        => $request->idaksi,
                    'idsubaksi'     => $request->idsubaksi,
                    'name'          => 'Aksi Donasi',
                    'quantity'      => 1,
                    'price'         => $request->besaran
                ),
            );

            $customer_details = array(
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
                'email'         => $request->email,
                'phone'         => $request->phone
            );

            if($request->payment_type == 'bank_transfer') {
                $payment = array(
                    "bank" => $request->bank
                );
                $transaction = array(
                    "payment_type" => $request->payment_type,
                    "bank_transfer" => $payment,
                    'transaction_details' => $transaction_details,
                    'customer_details' => $customer_details,
                    'item_details' => $items
                );
            }else if($request->payment_type == 'credit_card') {
                $transaction = array(
                    "payment_type" => $request->payment_type,
                    "credit_card" => array(
                        "token_id" => $request->token_id
                    ),
                    'transaction_details' => $transaction_details,
                    'customer_details' => $customer_details,
                    'item_details' => $items
                );
            }else if($request->payment_type == 'echannel') {
                $transaction = array(
                    "payment_type" => $request->payment_type,
                    'transaction_details' => $transaction_details,
                    'customer_details' => $customer_details,
                    'item_details' => $items,
                    "echannel" => array(
                        "bill_info1" => "Payment For:",
                        "bill_info2" => "debt"
                    )
                );
            }
            try{
                $response = Veritrans_VtDirect::charge($transaction);
                $idtransaksi = $response->transaction_id;

                if($request->payment_type == 'bank_transfer') {
                    $payment = $request->bank;
                    if($payment == 'bca' || $payment == 'bni'){
                        $va = $response->va_numbers[0]->va_number;
                    }else{
                        $va = $response->permata_va_number;
                    }
                }else if($request->payment_type == 'credit_card') {
                    $payment = 'credit_card';
                    $va = '';
                }else if($request->payment_type == 'echannel') {
                    $payment = 'Mandiri';
                    $va = $response->bill_key . '-' . $response->biller_code;
                }
                SubAksiAktivitasDonasiProses::create([
                    'idaksi'            => $request->idaksi,
                    'idsubaksi'         => $request->idsubaksi,
                    'iduser'            => $request->iduser,
                    'idpayment'         => $this->generatePaymentID($request->idaksi, $request->idsubaksi, $request->iduser),
                    'idtransaksi'       => $idtransaksi,
                    'nilaidonasi'       => $request->besaran,
                    'banktujuan'        => $payment,
                    'virtualaccount'    => $va,
                    'status'            => 0
                ]);
                return response()->json($response);
            }catch (\Exception $e){
                echo $e->getMessage();
                if(strpos ($e->getMessage(), "Access denied due to unauthorized")){
                    echo "<code>";
                    echo "<h4>Please set real server key from sandbox</h4>";
                    echo "In file: " . __FILE__;
                    echo "<br>";
                    echo "<br>";
                    echo htmlspecialchars('Veritrans_Config::$serverKey = \'SB-Mid-client-NyUH0f3so3Gviwq4\';');
                    die();
                }
            }
        }

        public function testingemail(){
            $user = 'riwandindi17@gmail.com';
            mail($user, 'Testing email', str_replace("\n.", "\n..", 'testing'));
        }

        public function success(Request $request){
            if($request->transaction_status == 'settlement'){
                $transaksi = SubAksiAktivitasDonasiProses::where('idtransaksi', $request->transaction_id);

                if($transaksi->count() != 0){
                    $transaksi->update(['status' => 1]);
                    $result = $transaksi->first();

                    $data['iduser']             = $result->iduser;
                    $data['idaksi']             = $result->idaksi;
                    $data['idsubaksi']          = $result->idsubaksi;
                    $data['idjenisaktivitas']   = 2;
                    $data['tanggalaktivitas']   = date("Y-m-d");
                    $data['waktuaktivitas']     = date("h:i:s");
                    $data['keterangan']         = 'Aksi Donasi';
                    $data['nilaidonasi']        = $result->nilaidonasi;
                    $data['keteranganDonasi']   = 'Aksi Donasi';
                    $data['idaktivitas']        = $this->getAktivitasId($data['iduser']);
                    $data['iddonasi']           = $this->getDonateId($data['idaksi'], $data['idsubaksi']);

                    $this->createDonate($data);
                    $res['success'] = true;
                    $res['message'] = "Berhasil menambahkan Donasi";
                    return response($res,200);
                }
            }else{
                return response("Bukan pembayaran",500);
            }
        }

        protected function getDonateId($idaksi, $idsubaksi){
            return count(SubAksiAktivitasDonasi::where('idaksi', $idaksi)->where('idsubaksi', $idsubaksi)->get()) + 1;
        }

        protected function getAktivitasId($id){
            return count(UserAktivitas::where('iduser', $id)->get()) + 1;
        }

        protected function createDonate(array $data){
            UserAktivitas::create([
                'iduser'            => $data['iduser'],
                'idaktivitas'       => $data['idaktivitas'],
                'idaksi'            => $data['idaksi'],
                'idsubaksi'         => $data['idsubaksi'],
                'idjenisaktivitas'  => $data['idjenisaktivitas'],
                'tanggalaktivitas'  => $data['tanggalaktivitas'],
                'waktuaktivitas'    => $data['waktuaktivitas'],
                'keterangan'        => $data['keterangan']
            ]);

            UserAktivitasDonasi::create([
                'iduser'        => $data['iduser'],
                'idaktivitas'   => $data['idaktivitas'],
                'nilaidonasi'   => $data['nilaidonasi'],
                'keterangan'    => $data['keteranganDonasi']
            ]);

            SubAksiAktivitasDonasi::create([
                'idaksi'        => $data['idaksi'],
                'idsubaksi'     => $data['idsubaksi'],
                'iddonasi'      => $data['iddonasi'],
                'iduser'        => $data['iduser'],
                'idaktivitas'   => $data['idaktivitas']
            ]);
        }
    }