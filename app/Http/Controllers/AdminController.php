<?php
    namespace App\Http\Controllers;
    use App\Aksi;
    use App\Event;
    use App\EventAksi;
    use App\EventJoin;
    use App\Grup;
    use App\GrupStatus;
    use App\SubAksiDonasi;
    use App\SubAksiPencairan;
    use App\SubAksiVolunteer;
    use App\User;
    use App\UserStatus;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;

    class AdminController extends Controller
    {
        public function listAnggota(){
            $user = User::where('iduser','<>',0)->with('userDetail')->with('userStatus')->with('userJurusan')
                ->offset(0)->limit(10)
                ->orderBy('fullname', 'asc')->get();
            $data['message'] = "Success";
            $data['data'] = $user;

            return response($data, 200);
        }

        public function searchAnggota(Request $request){
            $key = $request->get('key');
            $user = User::with('userDetail')->with('userStatus')->with('userJurusan')
                ->where('fullname','like','%' . $key . '%')->
                where('iduser','<>', 0)
                ->offset(0)->limit(10)
                ->orderBy('fullname', 'asc')->get();

            $data['data'] = $user;
            return response($data, 200);
        }

        public function blockAnggota($idUser){
            $user = UserStatus::where('iduser', $idUser);

            $update = $user->update([
                'status' => '2'
            ]);

            $data['message'] = "User berhasil di blok";
            $data['data'] = $user->get();

            return response($data, 200);
        }

        public function aktifAnggota($idUser){
            $user = UserStatus::where('iduser', $idUser);

            $update = $user->update([
                'status' => '1'
            ]);

            $data['message'] = "User berhasil di aktifkan kembali";
            $data['data'] = $user->get();

            return response($data, 200);
        }

        public function listPengajuan(){
            $q = "select idaksi, idsubaksi, idpencairan, tanggal, waktu, nominal, rekening, namabank, namapemilikrekening, namasubaksi, statuspencairan, keterangan as status 
                  from sub_aksi_pencairan 
                  inner join sub_aksi using(idaksi, idsubaksi) 
                  inner join sub_aksi_pencairan_status on sub_aksi_pencairan_status.status = statuspencairan 
                  limit 0, 10";

            $query = DB::select($q);

            $data['data'] = $query;

            return response($data, 200);
        }

        public function detailPengajuan(Request $request){
            $idAksi = $request->get('idaksi');
            $idSubaksi = $request->get('idsubaksi');
            $idPencairan = $request->get('idpencairan');

            $q = "select * from sub_aksi_pencairan 
                inner join sub_aksi_pencairan_status on sub_aksi_pencairan_status.status = statuspencairan 
                inner join sub_aksi using(idaksi, idsubaksi) 
                where idaksi = $idAksi and
                idsubaksi = $idSubaksi and 
                idpencairan = $idPencairan";

            $query = DB::select($q);
            $data['data'] = $query;

            return response($data, 200);
        }

        public function prosesPengajuan(Request $request){
            $idAksi = $request->get('idaksi');
            $idSubAksi = $request->get('idsubaksi');
            $idPencairan = $request->get('idpencairan');

            $query = SubAksiPencairan::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->where('idpencairan', $idPencairan);

            $query->update([
                'statuspencairan'   => 2
            ]);
            $data['data'] = $query->get();
            return response($data, 200);
        }

        public function searchPencairan(Request $request){
            $key = $request->get('key');
            $q = "select idaksi, idsubaksi, idpencairan, tanggal, waktu, nominal, rekening, namabank, namapemilikrekening, namasubaksi, statuspencairan, keterangan as status 
                  from sub_aksi_pencairan 
                  inner join sub_aksi using(idaksi, idsubaksi) 
                  inner join sub_aksi_pencairan_status on sub_aksi_pencairan_status.status = statuspencairan 
                  WHERE namasubaksi like '%$key%'
                  ORDER BY tanggal DESC, waktu DESC
                  limit 0, 10";

            $query = DB::select($q);

            $data['data'] = $query;

            return response($data, 200);
        }

        public function pencairan(Request $request, $cair){
            $idAksi = $request->get('idaksi');
            $idSubAksi = $request->get('idsubaksi');
            $idPencairan = $request->get('idpencairan');

            $query = SubAksiPencairan::where('idaksi', $idAksi)->where('idsubaksi', $idSubAksi)->where('idpencairan', $idPencairan);

            if($cair == 1){
                $query->update([
                    'statuspencairan'   => 3
                ]);
                $data['message'] = "Pencairan Berhasil";
                $data['data'] = $query->get();
            }else{
                $query->update([
                    'statuspencairan'   => 5
                ]);
                $data['message'] = "Pencairan Gagal";
                $data['data'] = $query->get();
            }
            return response($data, 200);
        }

        public function listGrup(){
            $grup = Grup::with('grupDetail')->with('grupStatus')->where('idgrup', '<>' , 0)
                ->offset(0)->limit(10)
                ->orderBy('namagrup', 'asc')->get();

            $data['data'] = $grup;
            return response($data, 200);
        }

        public function searchGrup(Request $request){
            $key = $request->get('key');
            $grup = Grup::with('grupDetail')->with('grupStatus')->where('namagrup','like','%' . $key . '%')
                ->offset(0)->limit(10)
                ->where('idgrup','<>', 0)->orderBy('namagrup', 'asc')->get();

            $data['data'] = $grup;
            return response($data, 200);
        }

        public function blockGrup($idUser){
            $grup = GrupStatus::where('idgrup', $idUser);

            $update = $grup->update([
                'status' => '2'
            ]);

            $data['message'] = "Grup berhasil di blok";
            $data['data'] = $grup->get();

            return response($data, 200);
        }

        public function aktifGrup($idUser){
            $grup = GrupStatus::where('idgrup', $idUser);

            $update = $grup->update([
                'status' => '1'
            ]);

            $data['message'] = "Grup berhasil di aktifkan kembali";
            $data['data'] = $grup->get();

            return response($data, 200);
        }

        public function listAksi(){
            $query = Aksi::where('tipeaksi',2)
                ->offset(0)->limit(10)
                ->orderBy('tanggalpembuatan', 'desc')->orderBy('waktupembuatan', 'desc')->get();
            $res['success'] = true;
            $res['message'] = "List Aksi";
            $res['data']    = $query;

            return response($res, 200);
        }

        public function searchAksi(Request $request){
            $key = $request->get('key');
            $query = Aksi::where('tipeaksi',2)
                ->offset(0)->limit(10)
                ->where('namaaksi', 'like', '%' . $key .'%')
                ->orderBy('tanggalpembuatan', 'desc')->orderBy('waktupembuatan', 'desc')->get();
            $data = $query;
            return response($data, 200);
        }

        public function blokAksi($idAksi){
            $aksi = Aksi::where('idaksi', $idAksi);

            $update = $aksi->update([
                'status' => '2'
            ]);

            $data['message'] = "Aksi berhasil di blok";
            $data['data'] = $aksi->get();

            return response($data, 200);
        }

        public function aktifAksi($idAksi){
            $aksi = Aksi::where('idaksi', $idAksi);

            $update = $aksi->update([
                'status' => '1'
            ]);

            $data['message'] = "Aksi berhasil di aktifkan kembali";
            $data['data'] = $aksi->get();

            return response($data, 200);
        }

        public function listAcara(){
            $event = Event::select('idevent','namaevent', 'lokasievent', 'status')
                ->offset(0)->limit(10)
                ->orderBy('tanggalpembuatan', 'desc')->orderBy('waktupembuatan', 'desc')->get();
            $res = $event;
            return response($res, 200);
        }

        public function searchAcara(Request $request){
            $key = $request->get('key');
            $query = Event::select('idevent','namaevent', 'lokasievent', 'status')
                ->where('namaevent', 'like', '%' . $key .'%')
                ->offset(0)->limit(10)
                ->orderBy('tanggalpembuatan', 'desc')->orderBy('waktupembuatan', 'desc')->get();
            $data = $query;
            return response($data, 200);
        }

        public function blokAcara($idAcara){
            $acara = Event::where('idevent', $idAcara);

            $update = $acara->update([
                'status' => '2'
            ]);

            $data['message'] = "Acara berhasil di blok";
            $data['data'] = $acara->get();

            return response($data, 200);
        }

        public function aktifAcara($idAcara){
            $event = Event::where('idevent', $idAcara);

            $update = $event->update([
                'status' => '1'
            ]);

            $data['message'] = "Acara berhasil di aktifkan kembali";
            $data['data'] = $event->get();

            return response($data, 200);
        }

        public function sendNotifikasi(Request $request){
            $message = $request->get('message');
            // \OneSignal::sendNotificationToAll(
            //     "$message",
            //     $url = null,
            //     $data = null,
            //     $buttons = null,
            //     $schedule = null
            // );
        }
    }