<?php
    namespace App\Http\Controllers;
    use App\DaftarJurusan;
    use App\UserJurusan;
    use App\UserRelasi;
    use Illuminate\Http\Request;
    class AlumniController extends Controller
    {
        public function getStrata(){
            $strata = DaftarJurusan::select('strata')->groupBy('strata')->get();
            $i=0;
            foreach ($strata as $s){
                $data[$i] = $s->strata;
                $i++;
            }

            return response($data, 200);
        }

        public function getJurusan(){
            $data = DaftarJurusan::all();

            return response($data, 200);
        }

        public function getUser(Request $request){
            $data = UserJurusan::with('user.userDetail');
            $data = $data->where('kode_jurusan', $request->jurusan);
            $data = $data->where('angkatan', $request->angkatan);
            $data = $data->whereNotIn('iduser', [$request->iduser]);
            $data = $data->get();

            $res = $data;

            $i=0;
            foreach($data as $list){
                $list = count(UserRelasi::where('iduser', $request->iduser)->where('iduser_following', $list->iduser)->get());
                if($list == 0){
                    $res[$i]['isFollowing'] = false;
                }else{
                    $res[$i]['isFollowing'] = true;
                }
                $i++;
            }

            return response($res, 200);
        }
    }