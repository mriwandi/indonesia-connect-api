<?php
    namespace App\Http\Controllers;

    use App\User;
    use App\UserRelasi;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    class FollowerController extends Controller
    {
        public function following($id){
            if(false){
                $res['error'] = true;
                $res['message'] = "Id user tidak ditemukan";
                return response($res, 500);
            }else{
                $followings = UserRelasi::where('iduser','=', $id)->get();
                $res['success'] = true;
                $res['message'] = "Data Following ditemukan";
                if(count($followings) != 0) {
                    $i = 0;
                    foreach ($followings as $following) {
                        $idfollowing = $following->iduser_following;
                        $user = User::where('iduser', '=', $idfollowing)->with('userDetail')->first();
                        $followed   = UserRelasi::where('iduser', $id)->where('iduser_following', $idfollowing)->get();
                        if(count($followed) != 0){
                            $data[$i]['followed'] = true;
                        }else{
                            $data[$i]['followed'] = false;
                        }
                        $data[$i]['iduser'] = $idfollowing;
                        $data[$i]['email'] = $user->email;
                        $data[$i]['fullname'] = $user->fullname;
                        $data[$i]['user_detail'] = $user->userDetail;
                        $i++;
                    }
                    $res['count'] = count($data);
                    $res['data'] = $data;
                }else{
                    $res['count'] = 0;
                    $res['data'] = [];
                }
                return response($res,200);
            }
        }

        public function follower($id){
            if(false){
                $res['error'] = true;
                $res['message'] = "Id user tidak ditemukan";
                return response($res, 500);
            }else{
                $followings = UserRelasi::where('iduser_following','=', $id)->get();
                $res['success'] = true;
                $res['message'] = "Data Follower ditemukan";
                if(count($followings) != 0){
                    $i = 0;
                    foreach($followings as $following){
                        $idfollowing = $following->iduser;
                        $user = User::where('iduser','=',$idfollowing)->first();
                        $followed   = UserRelasi::where('iduser', $id)->where('iduser_following', $idfollowing)->get();
                        if(count($followed) != 0){
                            $data[$i]['followed'] = true;
                        }else{
                            $data[$i]['followed'] = false;
                        }
                        $data[$i]['iduser'] = $idfollowing;
                        $data[$i]['email'] = $user->email;
                        $data[$i]['fullname'] = $user->fullname;
                        $data[$i]['user_detail'] = $user->userDetail;
                        $i++;
                    }
                    $res['count'] = count($data);
                    $res['data'] = $data;
                }else{
                        $res['count'] = 0;
                        $res['data'] = [];
                }
                return response($res,200);
            }
        }

        private function sendNotification($nama, $id){
            \OneSignal::sendNotificationUsingTags(
                "$nama menjadi follower Anda.",
                array(
                    ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $id]
                ),
                $url = null,
                $data = null,
                $buttons = null,
                $schedule = null
            );
        }

        public function follows(Request $request){
            $data['iduser'] = $request->get('iduser');
            $data['iduser_following'] = $request->get('iduser_following');
            $data['tanggalfollowing'] = date("Y-m-d");
            $data['waktufollowing'] = date("h:i:s");

            $input = UserRelasi::create([
                'iduser' => $data['iduser'],
                'iduser_following' => $data['iduser_following'],
                'tanggalfollowing' => $data['tanggalfollowing'],
                'waktufollowing' => $data['waktufollowing']
            ]);

            $nameUser = User::where('iduser', $data['iduser'])->first()->fullname;

            $this->sendNotification($nameUser, $data['iduser_following']);

            if($input){
                $res['success'] = true;
                $res['message'] = "Berhasil follow user";
                $code = 200;
                $res['data'] = $data;
            }else{
                $res['success'] = false;
                $res['message'] = "Gagal follow user";
                $code = 500;
                $res['data'] = $data;
            }
            
            return response($res, $code);
        }

        public function unfollow(Request $request){
            $iduser = $request->get('iduser');
            $idfollowing = $request->get('iduser_following');

            $relasi = UserRelasi::where('iduser',$iduser)->where('iduser_following', $idfollowing);
            $relasi->delete();

            if($relasi){
                $res['success'] = true;
                $res['message'] = "Berhasil unfollow user";
                $code = 200;
            }else{
                $res['success'] = false;
                $res['message'] = "Gagal unfollow user";
                $code = 500;
            }
            return response($res, $code);
        }

        public function cekRelasi(Request $request){
            $iduser = $request->get('iduser');
            $idfollowing = $request->get('iduser_following');

            $relasi = UserRelasi::where('iduser',$iduser)->where('iduser_following', $idfollowing)->get();

            if(count($relasi) != 0){
                $res['followed'] = true;
            }else{
                $res['followed'] = false;
            }
            
            $res['success'] = true;
            $res['message'] = "Followed Checked";

            return response($res,200);
        }

        public function search(Request $request){
            $q = $request->q;
            $query = User::where('fullname','like','%' . $q . '%')->with('userDetail')->where('iduser','<>', 0)->get();

            $res['success'] = true;
            $res['message'] = "List data user";
            $res['data']    = $query;

            return response($res, 200);
        }
    }