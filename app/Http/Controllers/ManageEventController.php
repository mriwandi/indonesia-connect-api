<?php
    namespace App\Http\Controllers;
    use App\Event;
    use App\EventJoin;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;

    class ManageEventController extends Controller
    {
        public function listAcara($idUser){
            $event = Event::select('idevent','namaevent', 'tanggalawalevent', 'waktuawalevent', 'tanggalakhirevent', 'waktuakhirevent','lokasievent','tanggalpembuatan','waktupembuatan')
                ->where('iduser_initiator', $idUser)->orderBy('idevent', 'desc')->get();
            $res = $event;
            $i = 0;
            foreach($event as $e){
                $currDate = date('Y-m-d');
                if($currDate < $e->tanggalawalevent){
                    $res[$i]['status'] = "Akan Datang";
                }else if($currDate > $e->tanggalakhirevent){
                    $res[$i]['status'] = "Sudah Berlalu";
                }else{
                    $res[$i]['status'] = "Sedang Berlangsung";
                }
                $res[$i]['peserta'] = count(EventJoin::where('idevent', $e->idevent)->get()) - 1;
                $i++;
            }
            return response($res, 200);
        }

        public function getAksiTerkait($idEvent){
            $query = "select * from sub_aksi where idaksi = (
                        select idaksi from event_aksi where idevent = 1
                    )";
            $data = DB::select(DB::raw($query));

            return response($data, 200);
        }
    }