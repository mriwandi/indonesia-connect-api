<?php
    namespace App\Http\Controllers;

    use App\User;
    use App\UserMessage;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use App\Http\Controllers\Controller;
    class MessagesController extends Controller
    {
        // public function getMessage($id){
        //     $messages = DB::select(DB::raw("SELECT * from user_message 
        //             where idpesan in (
        //                 select max(idpesan) from user_message where iduserpengirim = $id or iduserpenerima = $id GROUP BY idroom 
        //                 ORDER BY tanggalkirim, waktukirim desc
        //             ) ORDER BY idpesan desc"));
        //     $i = 0;
        //     foreach($messages as $m){
        //         if($m->iduserpenerima != $id){
        //             $messages[$i]->user = DB::select(DB::raw("Select iduser, fullname, urlfotoprofile, handphone from user inner join user_detail using(iduser) where iduser = $m->iduserpenerima"));
        //             $messages[$i]->status = 'mengirim';
        //         }else{
        //             $messages[$i]->user = DB::select(DB::raw("Select iduser, fullname, urlfotoprofile, handphone from user inner join user_detail using(iduser) where iduser = $m->iduserpengirim"));
        //             $messages[$i]->status = 'menerima';
        //         }
        //         $i++;
        //     }
        //     return response($messages, 200);
        // }

        public function getMessage($id){
            $messages = DB::select(DB::raw("SELECT * from user_message 
                    where idpesan in (
                        select max(idpesan) from user_message where iduserpengirim = $id or iduserpenerima = $id GROUP BY idroom 
                        ORDER BY tanggalkirim, waktukirim desc
                    ) ORDER BY idpesan desc"));
            $i = 0;
            $total = count($messages);
            for($i=0; $i < $total; $i++){
                if($messages[$i]->iduserpenerima != $id){
                    $query = "Select iduser, fullname, urlfotoprofile, handphone from user 
                                inner join user_detail using(iduser) where iduser = " . $messages[$i]->iduserpenerima;
                    $messages[$i]->user = DB::select(DB::raw($query));
                    $messages[$i]->status = 'mengirim';
                }else{
                    $query = "Select iduser, fullname, urlfotoprofile, handphone from user 
                                inner join user_detail using(iduser) where iduser = " .$messages[$i]->$iduserpengirim;
                    $messages[$i]->user = DB::select(DB::raw($query));
                    $messages[$i]->status = 'menerima';
                }
            }

            if($total == 0){
                $data['message'] = "Anda belum memiliki pesan";
                return response($data, 500);
            }else{
                return response($messages, 200);
            }
        }

//        public function get($id){
//            $messages = DB::select(DB::raw("SELECT * FROM user_message where (iduserpengirim, idpesan) in (SELECT iduserpengirim, max(idpesan) FROM user_message where iduserpengirim = '".$id."' GROUP BY iduserpenerima) ORDER BY tanggalkirim, waktukirim DESC"));
//            $res['success'] = true;
//            $res['message'] = "Data ditemukan";
//            if(count($messages) != 0) {
//                $i = 0;
//                foreach ($messages as $message) {
//                    $data[$i]['idpesan'] = $message->idpesan;
//                    $data[$i]['penerima'] = User::where('iduser','=',$message->iduserpenerima)->first();
//                    $data[$i]['isipesan'] = $message->isipesan;
//                    $data[$i]['tanggalkirim'] = $message->tanggalkirim;
//                    $data[$i]['waktukirim'] = $message->waktukirim;
//                    $data[$i]['statusbaca'] = $message->statusbaca;
//                    $i++;
//                }
//                $res['count'] = count($data);
//                $res['data'] = $data;
//            }else{
//                $res['count'] = 0;
//                $res['data'] = [];
//            }
//            return response($res,200);
//        }
//
//        public function getPerUser($id){
//            $idPenerima = $request->get('iduserpenerima');
//            $messages = UserMessage::where('iduserpengirim','=', $id)->where('iduserpenerima', $idPenerima)->get();
//            $res['success'] = true;
//            $res['message'] = "Data ditemukan";
//            if(count($messages) != 0) {
//                $i = 0;
//                foreach ($messages as $message) {
//                    $data[$i]['idpesan'] = $message->idpesan;
//                    $data[$i]['penerima'] = User::where('iduser','=',$message->iduserpenerima)->first();
//                    $data[$i]['isipesan'] = $message->isipesan;
//                    $data[$i]['tanggalkirim'] = $message->tanggalkirim;
//                    $data[$i]['waktukirim'] = $message->waktukirim;
//                    $data[$i]['statusbaca'] = $message->statusbaca;
//                    $i++;
//                }
//                $res['count'] = count($data);
//                $res['data'] = $data;
//            }else{
//                $res['count'] = 0;
//                $res['data'] = [];
//            }
//            return response($res,200);
//        }

        protected function getID(){
            return UserMessage::max('idpesan')+1;
        }

        public function send(Request $request, $idUser){
            $data['iduserpengirim'] = $idUser;
            $data['iduserpenerima'] = $request->get('iduser');
            $data['idpesan'] = $this->getID();
            $data['idroom'] = $request->get('idroom');
            $data['isipesan'] = $request->get('isipesan');
            $data['tanggalregistrasi'] = date("Y-m-d");
            $data['wakturegistrasi'] = date("h:i:s");

            $message = UserMessage::create([
                'iduserpengirim' => $data['iduserpengirim'],
                'iduserpenerima' => $data['iduserpenerima'],
                'idpesan' => $data['idpesan'],
                'idroom' => $data['idroom'],
                'isipesan' => $data['isipesan'],
                'tanggalkirim' => $data['tanggalregistrasi'],
                'waktukirim' => $data['wakturegistrasi'],
                'statusbaca' => 0
            ]);

            $namaUser = User::where('iduser', $data['iduserpengirim'])->first()->fullname;
            $this->sendNotification($namaUser, $data['iduserpenerima']);

            if($message){
                $res['success'] = true;
                $res['message'] = "Berhasil mengirim pesan";
                $res['data'] = $data;
                $code = 200;
            }else{
                $res['success'] = false;
                $res['message'] = "Gagal mengirim pesan";
                $code = 500;
            }
            
            return response($res, $code);
        }

        private function sendNotification($nama, $id){
            \OneSignal::sendNotificationUsingTags(
                "$nama menjadi follower Anda.",
                array(
                    ["field" => "tag", "key" => "idUser", "relation" => "=", "value" => $id]
                ),
                $url = null,
                $data = null,
                $buttons = null,
                $schedule = null
            );
        }

        public function getDetailMessage($idRoom){
            $query = "SELECT * FROM user_message WHERE idroom = $idRoom order by idpesan asc";
            $message = DB::select(DB::raw($query));

            if(count($message) != 0){
                $data['success'] = true;
                $data['messages'] = "Pesan tersedia";
                $data['data']   = $message;
                $code = 200;
            }else{
                $data['success'] = false;
                $data['messages'] = "Terjadi kesalahan dalam mengambil pesan";
                $code = 500;
            }
            return response($message, $code);
        }

        private function getNewIdRoom(){
            return UserMessage::max('idroom')+1;
        }

        public function newMessage(Request $request, $idUser){
            $idPenerima = $request->iduser;
            $query = UserMessage::where('iduserpengirim', $idUser)->where('iduserpenerima', $idPenerima);
            if($query->count() > 0){
                $data['idroom'] = $query->first()->idroom;
            }else{
                $data['idroom'] = $this->getNewIdRoom();
            }
            return response($data, 200);
        }
    }