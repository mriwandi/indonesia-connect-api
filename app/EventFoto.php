<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class EventFoto extends Model
{
    protected $table = 'event_foto';
    protected $primaryKey = 'idfoto';
    public $timestamps = false;

    protected $fillable = [
        'idevent',
        'idfoto',
        'urlfotoevent'
    ];
}