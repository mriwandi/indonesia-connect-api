<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class UserAktivitas extends Model
{
    use Compoships;

    protected $table = 'user_aktivitas';
    protected $primaryKey =  'idaktivitas';
    public $timestamps = false;

    protected $hidden = [
        'idaksi','idsubaksi','idjenisaktivitas'
    ];

    protected $fillable = [
        'iduser',
        'idaktivitas',
        'idaksi',
        'idsubaksi',
        'idjenisaktivitas',
        'tanggalaktivitas',
        'waktuaktivitas',
        'keterangan'
    ];

    public function subAksi(){
        return $this->hasOne('App\SubAksi','idsubaksi');
    }

    public function userAktivitasUsul(){
        return $this->hasOne('App\UserAktivitasUsul', ['iduser','idaktivitas'], ['iduser','idaktivitas']);
    }


    public function userAktivitasDonasi(){
        return $this->hasOne('App\UserAktivitasDonasi', 'idaktivitas');
    }

    public function userAktivitasVolunteer(){
        return $this->hasOne('App\UserAktivitasVolunteer', 'idaktivitas');
    }

    public function user(){
        return $this->belongsTo('App\User','iduser','iduser');
    }

    public function aksi(){
        return $this->hasOne('App\Aksi','idaksi','idaksi');
    }

    public function jenisAktivitas(){
        return $this->hasOne('App\JenisAktivitas','idjenisaktivitas','idjenisaktivitas');
    }
}