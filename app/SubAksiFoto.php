<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class SubAksiFoto extends Model
{
    use Compoships;

    protected $table = 'sub_aksi_foto';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idsubaksi',
        'idfoto',
        'urlfotosubaksi'
    ];
}