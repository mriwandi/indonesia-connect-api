<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SubAksiAktivitasDonasiProses extends Model
{
    protected $table = 'sub_aksi_aktivitas_donasi_proses';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idsubaksi',
        'iduser',
        'idpayment',
        'idtransaksi',
        'nilaidonasi',
        'banktujuan',
        'virtualaccount',
        'status',
        'token_id',
        'payment_type',
        'payment_status',
        'order_id'
    ];

}