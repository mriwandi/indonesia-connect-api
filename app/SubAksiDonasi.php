<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class SubAksiDonasi extends Model
{
    use Compoships;

    protected $table = 'sub_aksi_donasi';
    protected $primaryKey = 'id_subaksi';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idsubaksi',
        'tanggalmulai',
        'tanggalselesai',
        'targetdonasi',
    ];
}