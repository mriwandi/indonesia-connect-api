<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class AksiUsulVote extends Model
{
    protected $table = 'aksi_usul_vote';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idusul',
        'idvote',
        'iduservote',
        'tanggalvote',
        'waktuvote',
        'isvoteaktif'
    ];
}