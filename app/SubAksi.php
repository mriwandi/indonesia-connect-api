<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;
class SubAksi extends Model
{
    use Compoships;

    protected $table = 'sub_aksi';
    protected $primaryKey = 'idsubaksi';
    public $timestamps = false;

    protected $hidden = [

    ];

    protected $fillable = [
        'idaksi','idsubaksi','idjenisaksi','namasubaksi','deskripsisubaksi','tanggalpembuatan','waktupembuatan'
    ];

    public function aksi(){
        return $this->belongsTo('App\Aksi', 'idaksi');
    }

    public function jenisAksi(){
        return $this->belongsTo('App\JenisAksi','idjenisaksi', 'idjenisaksi');
    }

    public function subAksiDonasi(){
        return $this->hasOne('App\SubAksiDonasi',['idaksi', 'idsubaksi'],['idaksi', 'idsubaksi']);
    }

    public function subAksiVolunteer(){
        return $this->hasOne('App\SubAksiVolunteer',['idaksi', 'idsubaksi'],['idaksi', 'idsubaksi']);
    }

    public function aktivitasSubAksi(){
        return $this->hasMany('App\UserAktivitas',['idaksi', 'idsubaksi'],['idaksi', 'idsubaksi']);
    }

    public function subAksiFoto(){
        return $this->hasMany('App\SubAksiFoto', ['idaksi', 'idsubaksi'],['idaksi', 'idsubaksi']);
    }
//
//    public function aksi(){
//        return $this->hasMany('App\Aksi');
//    }
//
//    public function sub_aksi_usul(){
//        return $this->hasMany('App\SubAksiUsul');
//    }
//
//    public function user_aktivitas(){
//        return $this->hasMany('App\UserAktivitas');
//    }
}