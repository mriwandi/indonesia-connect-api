<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class UserJurusan extends Model
{
    use Compoships;

    protected $table = 'user_jurusan';
    public $timestamps = false;

    protected $fillable = [
        'iduser',
        'strata',
        'kode_jurusan',
        'angkatan'
    ];

    public function user(){
        return $this->hasOne('App\User', 'iduser', 'iduser');
    }

    public function jurusan(){
        return $this->hasOne('App\DaftarJurusan', 'kode_jurusan', 'kode_jurusan');
    }
}