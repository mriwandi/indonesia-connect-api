<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class JenisAksi extends Model
{
    use Compoships;

    protected $table = 'jenis_aksi';
    protected $primaryKey = 'idjenisaksi';
    public $timestamps = false;

    public function sub_aksi(){
        return $this->hasMany('App\SubAksi','idjenisaksi');
    }
}