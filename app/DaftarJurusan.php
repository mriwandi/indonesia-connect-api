<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class DaftarJurusan extends Model
{
    use Compoships;
    
    protected $table = 'daftar_jurusan';
    public $timestamps = false;

    protected $fillable = [
        'strata',
        'kode_jurusan',
        'kode_jurusan_lama',
        'nama_jurusan',
        'fakultas'
    ];
}