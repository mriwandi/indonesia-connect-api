<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class UserRelasi extends Model
{
    protected $table = 'user_relasi';
    public $timestamps = false;

    protected $fillable = [
        'iduser','iduser_following','tanggalfollowing','waktufollowing'
    ];

    public function userDetail(){
        return $this->hasOne('App\UserDetail', 'iduser', 'iduser');
    }
}