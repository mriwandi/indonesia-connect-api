<?php

namespace App;

use Awobaz\Compoships\Compoships;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, Compoships;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user';
    protected $primaryKey = 'iduser';
    public $timestamps = false;

    protected $fillable = [
        'iduser','email','fullname','password','tanggalregistrasi','wakturegistrasi'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function userDetail(){
        return $this->hasOne('App\UserDetail', 'iduser', 'iduser');
    }

    public function userJurusan(){
        return $this->hasOne('App\UserJurusan', 'iduser', 'iduser');
    }

    public function userRelasi(){
        return $this->hasMany('App\UserRelasi', 'iduser', 'iduser');
    }

    public function userStatus(){
        return $this->hasOne('App\UserStatus', 'iduser', 'iduser');
    }
}
