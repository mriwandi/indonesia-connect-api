<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class AksiReportDeskriptif extends Model
{
    protected $table = 'aksi_report_deskriptif';
    protected $primaryKey = 'idreport';
    public $timestamps = false;

    protected $fillable = [
        'idaksi',
        'idreport',
        'deskripsi'
    ];
}