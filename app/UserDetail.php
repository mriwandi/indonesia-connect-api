<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class UserDetail extends Model
{
    use Compoships;

    protected $table = 'user_detail';
    protected $primaryKey = 'iduser';
    public $timestamps = false;

    protected $fillable = [
        'iduser','urlfotoprofile','handphone','lokasi','bio', 'profesi', 'perusahaan'
    ];
}