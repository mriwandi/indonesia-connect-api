<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SubAksiAktivitasDonasi extends Model
{
    protected $table = 'sub_aksi_aktivitas_donasi';
    public $timestamps = false;

    protected $hidden = [

    ];

    protected $fillable = [
        'idaksi',
        'idsubaksi',
        'iddonasi',
        'iduser',
        'idaktivitas'
    ];
}