<?php
namespace App;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
class JenisAktivitas extends Model
{
    use Compoships;

    protected $table = 'jenis_aktivitas';
    protected $primaryKey = 'idjenisaktivitas';
    public $timestamps = false;
}