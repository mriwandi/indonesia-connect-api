<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class EventInvitation extends Model
{
    protected $table = 'event_invitation';
    public $timestamps = false;

    protected $fillable = [
        'idevent',
        'iduser_invited',
        'iduser_inviter',
        'status'
    ];

    public function grupDetail(){
        return $this->hasOne('App\GrupDetail','idgrup');
    }
}