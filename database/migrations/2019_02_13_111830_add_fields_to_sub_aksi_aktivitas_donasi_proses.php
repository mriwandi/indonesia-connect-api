<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSubAksiAktivitasDonasiProses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_aksi_aktivitas_donasi_proses', function (Blueprint $table) {
            $table->string('token_id')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('payment_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_aksi_aktivitas_donasi_proses', function (Blueprint $table) {
            $table->dropColumn('token_id');
            $table->dropColumn('payment_type');
            $table->dropColumn('payment_status');
        });
    }
}
